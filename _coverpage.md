![logo](_media/logo.png)

# Sweet-API <small>1.0.0</small>

> 基于Spring Boot的插件化HTTP API快速接口开发框架，线上完成接口开发、调试和发布。

- 快速、轻便
- 多租户，多数据源管理
- 默认提供丰富插件
- 可拔插的插件扩展

[Github](https://github.com/zhouzhihu/sweet-api)
[Gitee](https://gitee.com/s-sweet/sweet-api)
[Get Started](/pages/)