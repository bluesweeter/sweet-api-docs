# 简介 <!-- {docsify-ignore-all} -->
Sweet-API是基于Java的HTTP API快速接口开发框架，线上完成接口开发、调试和发布，自动映射为HTTP接口。 无需定义Controller、Service、Dao、Mapper、XML、VO等Java对象即可完成常见的HTTP API接口开发。

## 特性
* 支持MySQL、MariaDB、Oracle、DB2、PostgreSQL、SQLServer等支持jdbc规范的数据库
* 支持非关系型数据库Redis、Mongodb、ElasticSearch
* 支持动态配置定时任务
* 支持集群部署、接口自动同步
* 支持分页查询以及自定义分页查询
* 支持多数据源配置，支持在线配置数据源
* 支持SQL缓存，以及自定义SQL缓存
* 支持自定义JSON结果、自定义分页结果