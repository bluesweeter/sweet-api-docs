## 请求参数获取 <!-- {docsify-ignore-all} -->

### RequestParam

```text
GET http://localhost/xx/xx?name=zhouzhihu&age=40
```

这样的`URL`参数`Sweet-API` 会自动将`name`和`age`映射为同名变量，在脚本中书写如下：

```js
this.name; //获取请求参数`姓名`
this.age; // 获取请求参数`年龄`
```

`Sweet-API`界面`param`参数设置界面：

![请求参数](../../_media/quick/requestParam.png ':size=60%')

### 表单参数

```text
POST http://localhost/xxx/xxx
name=abc&age=49
```

这样的表单参数`Sweet-API` 也会自动将`name`和`age`映射为同名变量，在脚本中书写如下：

```js
this.name; //获取请求参数`姓名`
this.age; // 获取请求参数`年龄`
```

### Request Header参数获取  

`Sweet-API` 会对所有`RequestHeader`统一封装为一个名为`header`的`Map`变量 如要获取 `Content-Type` 可以通过`this.header.get("Content-Type")` 来获取

```js
return this.header.get("Content-Type");
```

`Sweet-API`界面`header`参数设置界面：

![请求参数](../../_media/quick/requestHeader.png ':size=60%')

### Path参数获取

主要是针对URL定义为`http://localhost/user/{id}` 的类似接口，如要获取`path`路径上的`id`可通过`this.path.id`来获取。

```js
return this.path.id;
```

`Sweet-API`界面`path`参数设置界面：

![请求参数](../../_media/quick/requestPath.png ':size=60%')

### Cookie参数获取

`Sweet-API` 会对所有`Cookie`统一封装为一个名为`cookie`的`Map`对象。 如要获取 `JSESSIONID` 可以通过`cookie.get("JSESSIONID")`来获取。

### Session参数获取

`Sweet-API` 会将`HttpSession`封装为一个名为`session`的变量 要获取`session`中的值，可以通过`session.get("xxx")`来获取

### 注意事项

> 如果脚本自定义变量和参数变量冲突，自定义变量优先。