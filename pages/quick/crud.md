
## 表结构

以下以MySQL为例，创建一个`新闻表`：

```sql
CREATE TABLE `news`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '唯一ID',
  `subject` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 113 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic; 
```

## 前提

### 1）了解db模块

`Sweet-API`内置`db模块`能够帮助我们快速、方便的操作数据库，以下是一些简单的接口说明，具体详细情况参考：[db模块](/pages/api/db)。

```js
/**
 * 插入数据
 * @param {string} insertSql 插入SQL语句
 * @param {Map<String, Object>} params 参数
 * @returns 插入数据主键值
 */
this.db.insert(insertSql, params);

/**
 * 查询数据
 * @param {string} selectSql 查询SQL语句
 * @param {Map<String, Object>} params 参数
 * @returns {List<Map<String, Object>} 数据列表
 */
this.db.select(selectSql, params);

/**
 * 更新数据
 * @param {string} updateSql 更新SQL语句
 * @param {Map<String, Object>} params 参数
 * @returns {number} 执行SQL后影响行数
 */
this.db.update(updateSql, params);

/**
 * 删除数据
 * @param {string} updateSql 删除SQL语句
 * @param {Map<String, Object>} params 参数
 * @returns {number} 执行SQL后影响行数
 */
this.db.update(deleteSql, params);

/**
 * @typedef {Object} PageInfo 分页信息
 * @property {number} page 当前页
 * @property {number} pages 总页数
 * @property {number} limit 总页数限制条数
 * @property {number} total 总条数
 * @property {Object} records 数据列表
 * @property {string} ascs 正序排序字段
 * @property {string} descs 倒序排序字段
 */

/**
 * 获取分页
 * @param {string} pageSql 分页SQL语句
 * @param {long} page 页码
 * @param {long} limit 每页条数
 * @returns {PageInfo} 分页信息
 */
this.db.page1(pageSql, page, limit);

/**
 * 获取分页
 * @param {string} pageSql 分页SQL语句
 * @param {long} page 页码
 * @param {long} limit 每页条数
 * @param {Map<String, Object>} params 参数
 * @returns {PageInfo} 分页信息
 */
this.db.page1(pageSql, page, limit, params);

```

### 2）SQL参数

#### ${}拼接参数

作用和`mybatis`一致，都是将`${}`区域替换为对应的变量值

```js
let id = 92;
this.db.select("select * from news where id = ${id}", {id: id});
```

运行时生成的`SQL`为：select * from news where id = 92;

#### 动态SQL参数

通过`?{condition,expression}`来实现动态拼接SQL，如果条件成立则拼接后部分内容SQL中

```js
let id = null
this.db.select("select * from news ?{id, where id = ${id}}", {id: id});
```

如果`id`为`null`或`undefined`，则运行时生成的SQL为：`select * from news`。

```js
let id = 110
this.db.select("select * from news ?{id, where id = ${id}}", {id: id});
```

如果`id`存在，则运行时生成的SQL为：`select * from news where id = 110`

## 增删改查

了解上面的知识点后，进行表的增删改查就很容易书写脚本了。

### 1）插入数据

由于`news`表的`id`字段是自增的，所有插入时可以不指定`id`的值。

```js
this.db.insert("insert into news (subject, create_time, content) values ('${subject}', '${createTime}', '${content}')", { 
    subject: '新闻标题', 
    createTime: '2023-05-07 11:01:12', 
    content: '新闻内容'
});
```

运行时生成的SQL为：`insert into news (subject, create_time, content) values ('新闻标题', '2023-05-07 11:01:12', '新闻内容')`

返回值为插入数据的`主键值`

### 2）查询数据

```js
this.db.select("select * from news where id = ${id}", {id: 110});
```

运行时生成的SQL为：`select * from news where id = 110`

返回值为：`List<Map<String, Object>>` 类型列表

### 3）更新数据

```js
this.db.update("update news set subject = '${subject}' where id = ${id}", {id: 110, subject: "修改新闻标题"});
```

运行时生成的SQL为：`update news set subject = '修改新闻标题' where id = 110`

返回值为：执行SQL影响的行数，如果没有更新成功则返回`0`

### 4）删除数据

```js
this.db.update("delete from news where id = ${id}", {id : 110});
```

运行时生成的SQL为：`delete from news where id = 110`

返回值为：执行SQL影响的行数，如果没有更新成功则返回`0`

### 5）分页数据

由于不同的数据库对分页的SQL要求不一样，在获取分页时，会自动根据不同的数据库引擎生成不同的分页SQL

```js
this.db.page1("select * from news", 1, 10);
this.db.page1("select * from news where subject like '%${subject}%'", 1, 10, {subject: "新闻"})
```