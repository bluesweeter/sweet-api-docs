
## 表结构

以下以MySQL为例，还是以`新闻表`为例，具体表结构如下：

```sql
CREATE TABLE `news`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '唯一ID',
  `subject` varchar(4000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 113 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic; 
```

## 前提

### 了解db模块table

`Sweet-API`内置`db模块`提供了对单表`table`操作接口封装，具体详细情况参考：[db模块](/pages/api/db)。

操作入口：`db.table('table_name')`，例如：`this.db.table("news")`来指定操作`news`表

```js
/**
 * 插入数据
 * @param {Map<String, Object>} data 插入的数据
 * return 插入数据主键值
 */
this.db.table("news").insert(data);

/**
 * 查询数据
 * @returns {List<Map<String, Object>} 表所有数据列表
 */
this.db.table("news").select();

/**
 * 更新数据,配合`where`或指定更新数据的`主键值`才运行
 * @param {Map<String, Object>} data 更新数据
 * @returns {number} 执行SQL后影响行数
 */
this.db.table("news").update(data);

/**
 * 删除数据,需要配合`where`才运行
 * @returns {number} 执行SQL后影响行数
 */
this.db.table("news").delete();

/**
 * @typedef {Object} PageInfo 分页信息
 * @property {number} page 当前页
 * @property {number} pages 总页数
 * @property {number} limit 总页数限制条数
 * @property {number} total 总条数
 * @property {Object} records 数据列表
 * @property {string} ascs 正序排序字段
 * @property {string} descs 倒序排序字段
 */

/**
 * 获取分页
 * @param {long} page 页码
 * @param {long} limit 每页条数
 * @returns {PageInfo} 分页信息
 */
this.db.table("news").page1(page, limit);

```

## 增删改查

了解上面的知识点后，进行`单表table`的增删改查就很容易书写脚本了。

### 1）单表插入

由于`news`表的`id`字段是自增的，所有插入时可以不指定`id`的值。

```js
this.db.table("news").insert({subject: "单表插入数据", create_time: "2023-05-07 11:20:30", content: "单表插入数据"});
```

运行时生成的SQL为：`insert into news(create_time,subject,content) values (?,?,?)`

SQL参数：`2023-05-07 11:20:30(String), 单表插入数据(String), 单表插入数据(String)`

返回值为插入数据的`主键值`

### 2）单表查询

```js
this.db.table("news").select();
```

运行时生成的SQL为：`select * from news`

返回值为：`List<Map<String, Object>>` 类型列表

### 3）单表更新

> 在更新单表数据时，为避免误操作更新了所有数据，所以要求在更新时配合`where`或指定更新数据的`主键值`才运行，否则报异常错误。

```js
this.db.table("news").update({subject: "单表更新"});
```

抛出异常：

![更新异常](../../_media/quick/updateException.png)

```js
// 指定主键更新
this.db.table("news").primary("id").update({id: 129, subject: "单表更新"});
// where条件更新
result = this.db.table("news").where().eq("id", 129).update({subject: "单表更新"});
```

以上两种方式都可以，运行时生成的SQL为：`update news set subject = ? where id  = ?`

SQL参数：`单表更新(String), 129(Integer)`


### 4）单表删除

> 在删除单表数据时，为避免误操作删除了所有数据，所以要求在删除时配合`where`才运行，否则报异常错误。

```js
this.db.table("news").delete();
```

抛出异常:

![更新异常](../../_media/quick/deleteException.png)

```js
this.db.table("news").where().eq("id", 129).delete();
```

运行时生成的SQL为：`delete from news where id  = ?`

SQL参数：`129(Integer)`

### 5）单表分页

由于不同的数据库对分页的SQL要求不一样，在获取分页时，会自动根据不同的数据库引擎生成不同的分页SQL

```js
this.db.table("news").page1(1, 10);
```