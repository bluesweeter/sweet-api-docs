## 事务  <!-- {docsify-ignore-all} -->

在数据库操作过程中，有些业务无可避免的需要使用到事务，`db模块`也提供对事务支持

通过`db.transaction(回调函数)`可以进行事务操作，在`回调函数`中所有对数据库的新增、删除、更新都在同一个事务中。

```js
/**
 * 开启事务
 * @param {function} func 回调函数，回调函数有一个参数是带事务的db对象
 * @returns {Object} 执行结果
 */
this.db.transaction(func);
```

以下示例采用事务执行了两条SQL：

* 删除id为132的记录

* 插入一条id为139的记录

由于第二条插入的记录`id`已经在数据库中，运行时会抛出`异常`，因为使用了事务，所以上面删除也会被回滚

```js
// 事务
this.db.transaction((tranDb)=>{
    tranDb.table("news").where().eq("id", 132).delete();
    tranDb.table("news").insert({id: 139, subject: "单表插入数据", create_time: "2023-05-07 11:20:30", content: "单表插入数据"});
});
```