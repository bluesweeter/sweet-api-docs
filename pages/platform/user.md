> `User模块`是对接平台RAM服务的用户管理模块接口，在代码中的关键字为 `this.userService`。

## API说明  <!-- {docsify-ignore-all} -->
***

```js
/**
 * 入参：userName: String 用户名
 * 返回值：cn.hutool.json.JSONObject 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamUser
 * 函数说明：根据登录名获取启用状态的用户
 */
this.userService.getByUserName(userName);

/**
 * 入参：userNames: String[] 用户名集合
 * 返回值：cn.hutool.json.JSONArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamUser
 * 函数说明：根据登录名获取启用状态的用户集合
 */
this.userService.listByUserNames(...userNames);

/**
 * 入参：userId: Object 用户ID
 * 返回值：cn.hutool.json.JSONObject 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamUser
 * 函数说明：根据用户ID获取启用状态的用户
 */
this.userService.getById(userId);

/**
 * 入参：userIds: Object[] 用户ID集合
 * 返回值：cn.hutool.json.JSONArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamUser
 * 函数说明：根据登录名获取启用状态的用户集合
 */
this.userService.listByIds(...userIds);

/**
 * 入参：userName: String 用户名
 * 返回值：cn.hutool.json.JSONObject 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamUserInfoVO
 * 函数说明：根据用户名获取启用状态的用户信息
 */
this.userService.getUserInfoByUserName(userName);

/**
 * 入参：userNames: String[] 用户名集合
 * 返回值：cn.hutool.json.JSONArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamUserInfoVO
 * 函数说明：根据用户名获取启用状态的用户信息集合
 */
this.userService.listUserInfoByUserNames(...userNames);

/**
 * 入参：ouId: Object[] 组织ID集合
 * 返回值：cn.hutool.json.JSONArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamUser
 * 函数说明：根据组织ID加载用户集合
 */
this.userService.getOuInfoUsers(ouId);

/**
 * 入参：groupId: Object[] 岗位ID集合
 * 返回值：cn.hutool.json.JSONArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamUser
 * 函数说明：根据岗位ID加载用户集合
 */
this.userService.getGroupUsers(groupId);

/**
 * 入参：ouInfoScopeType: Int 组织范围 (0:全部;1:部门;2:单位;3:下级单位(下管一层);4:上级单位(纵向向上一层);)
 * 入参：groupCodes: String[] 岗位编码集合
 * 返回值：cn.hutool.json.JSONArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamUserInfoVO
 * 函数说明：根据指定组织范围和岗位编码加载用户信息集合
 */
this.userService.getUserInfoByGroupCode(ouInfoScopeType, ...groupCodes);

/**
 * 入参：ouInfoScopeType: Int 组织范围 (0:全部;1:部门;2:单位;3:下级单位(下管一层);4:上级单位(纵向向上一层);)
 * 入参：groupNames: String[] 岗位名称集合
 * 返回值：cn.hutool.json.JSONArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamUserInfoVO
 * 函数说明：根据指定组织范围和岗位名称加载用户信息集合
 */
this.userService.getUserInfoByGroupCode(ouInfoScopeType, ...groupNames);

/**
 * 入参：ouInfoScopeType: Int 组织范围 (0:全部;1:部门;2:单位;3:下级单位(下管一层);4:上级单位(纵向向上一层);)
 * 入参：roleNames: String[] 角色名称集合
 * 返回值：cn.hutool.json.JSONArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamUserInfoVO
 * 函数说明：根据指定组织范围和角色名称加载用户信息集合
 */
this.userService.getUserInfoByRoleName(ouInfoScopeType, ...roleNames);
```