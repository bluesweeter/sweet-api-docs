> `Atm模块`是对接平台文件服务模块接口，在代码中的关键字为 this.atmService

## API说明  <!-- {docsify-ignore-all} -->
***

```js
/**
 * 入参：file : MultipartFile 上传文件(可以通过request.getFile获取)
 * 入参：unid : String unid
 * 入参：type : String 附件类型
 * 返回值： cn.hutool.json.JSONObject 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginAttachment
 * 函数说明：上传附件
 */
this.atmService.upload(file, unid, type);

/**
 * 入参：parentUnid: String 父UNID
 * 入参：parentType: String 父类型，为空代表所有类型
 * 返回值：Boolean 是否删除成功
 * 函数说明：删除指定类型附件
 */
this.atmService.delete(parentUnid, parentType);

/**
 * 入参：parentUnid: String 父UNID
 * 入参：parentType: List<String> 父类型，为空代表所有类型
 * 返回值：cn.hutool.json.JSONOArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginAttachment
 * 函数说明：删除指定类型附件
 */
this.atmService.list(parentUnid, ...parentType);

/**
 * 入参：id:String[] 附件id集合
 * 返回值：Feign.Response Feign响应
 * 函数说明：根据附件id下载附件,多个附件下载zip
 */
this.atmService.downloadById(...id);

/**
 * 入参：unid:String[] 附件unid集合
 * 返回值：Feign.Response Feign响应
 * 函数说明：根据附件unid下载附件,多个附件下载zip
 */
this.atmService.downloadByUnid(...unid);
```