> `Context模块`记录系统上下文信息，包括当前登录用户信息，当前租户信息等；

## API说明  <!-- {docsify-ignore-all} -->
***

```js
/**
 * 租户编码
 */
this.context.tenant;

/**
 * 用户ID
 */
this.context.id;

/**
 * 用户登录名称
 */
this.context.name;

/**
 * 用户中文名
 */
this.context.nick;

/**
 * 用户类型
 */
this.context.type;

/**
 * 组织ID
 */
this.context.ouId;

/**
 * 组织名称
 */
this.context.ouName;

/**
 * 组织编码
 */
this.context.ouCode;

/**
 * 组织全编码
 */
this.context.ouFullCode;

/**
 * 组织全名称
 */
this.context.ouFullName;

/**
 * 单位ID
 */
this.context.unitId;

/**
 * 单位名称
 */
this.context.unitName;

/**
 * 单位编码
 */
this.context.unitCode;

/**
 * 单位全编码
 */
this.context.unitFullCode;

/**
 * 单位全名称
 */
this.context.unitFullName;

/**
 * 入参：modelCode:String 资源编码
 * 入参：dataPriviCode:String 权限编码
 * 返回值：Boolean
 * 函数说明：判断当前用户是否拥有指定数据权限
 */
this.context.isHaveResource("modelCode", "dataPriviCode");
```