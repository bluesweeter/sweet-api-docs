> `Work模块`是对接平台工作事项服务模块接口，在代码中的关键字为 this.workService

## API说明  <!-- {docsify-ignore-all} -->
***

```js
/**
 * 入参： docType: String 文档类型
 * 入参： subject: String 待办主题
 * 入参： description: String 待办描述
 * 入参： srcFileId: Object 源文档ID
 * 入参： userIds: Object[] 待办人
 * 返回值：Boolean 是否成功
 * 函数说明：发送待办给多个待办人
 */
this.workService.sendToDoWork(docType, subject, description, srcFileId, srcFileArgs, ...userIds);

/**
 * 入参： docType: String 文档类型
 * 入参： subject: String 待办主题
 * 入参： description: String 待办描述
 * 入参： srcFileId: Object 源文档ID
 * 入参： flowInstanceId: String 流程实例ID
 * 入参： userIds: Object[] 待办人
 * 返回值：Boolean 是否成功
 * 函数说明：发送流程待办给多个待办人
 */
this.workService.sendToDoWork(docType, subject, description, srcFileId, srcFileArgs, flowInstanceId, ...userIds);

/**
 * 入参： docType: String 文档类型
 * 入参：srcFileId: Object 源文件ID
 * 返回值：是否成功
 * 函数说明：删除指定源文件ID的所有待办信息
 */
this.workService.deleteToDoWork(docType, srcFileId);

/**
 * 入参： docType: String 文档类型
 * 入参：srcFileId: Object 源文件ID
 * flowInstanceId: String 流程实例ID
 * 返回值：是否成功
 * 函数说明：删除指定流程待办信息
 */
this.workService.deleteToDoWork(docType, srcFileId, flowInstanceId);

/**
 * 入参： docType: String 文档类型
 * 入参：srcFileId: Object 源文件ID
 * 入参： userIds: Object[] 待办人
 * 返回值：是否成功
 * 函数说明：删除指定人员待办信息
 */
this.workService.deleteUserToDoWork(docType, srcFieldId, ...userIds);

/**
 * 入参： docType: String 文档类型
 * 入参：srcFileId: Object 源文件ID
 * 入参：flowInstanceId: String 流程实例ID
 * 入参： userIds: Object[] 待办人
 * 返回值：是否成功
 * 函数说明：删除指定流程中指定人员待办信息
 */
this.workService.deleteUserToDoWork(docType, srcFieldId, flowInstanceId, ...userIds);

/**
 * 入参： docType: String 文档类型
 * 入参：srcFileId: Object 源文件ID
 * 返回值：是否成功
 * 函数说明：完成指定源文件ID待办信息
 */
this.workService.finishToDoWork(docType, srcFileId);

/**
 * 入参： docType: String 文档类型
 * 入参：srcFileId: Object 源文件ID
 * 入参：flowInstanceId: String 流程实例ID
 * 返回值：是否成功
 * 函数说明：完成指定流程待办信息
 */
this.workService.finishToDoWork(docType, srcFileId, flowInstanceId);

/**
 * 入参： docType: String 文档类型
 * 入参：srcFileId: Object 源文件ID
 * 入参： userIds: Object[] 待办人
 * 返回值：是否成功
 * 函数说明：完成指定人员待办信息
 */
this.workService.finishUserToDoWork(docType, srcFieldId, ...userIds);

/**
 * 入参： docType: String 文档类型
 * 入参：srcFileId: Object 源文件ID
 * 入参：flowInstanceId: String 流程实例ID
 * 入参： userIds: Object[] 待办人
 * 返回值：是否成功
 * 函数说明：完成指定流程中指定人员待办信息
 */
this.workService.finishUserToDoWork(docType, srcFieldId, flowInstanceId, ...userIds);

/**
 * 入参： docType: String 文档类型
 * 入参：srcFileId: Object 源文件ID
 * 返回值：cn.hutool.json.JSONOArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginWorkToDo
 * 函数说明：查询指定源文件ID待办信息
 */
this.workService.listToDoWork(docType, srcFileId);

/**
 * 入参： docType: String 文档类型
 * 入参：srcFileId: Object 源文件ID
 * 入参：flowInstanceId: String 流程实例ID
 * 返回值：cn.hutool.json.JSONOArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginWorkToDo
 * 函数说明：查询指定流程待办信息
 */
this.workService.listToDoWork(docType, srcFileId, flowInstanceId);

/**
 * 入参： docType: String 文档类型
 * 入参：srcFileId: Object 源文件ID
 * 返回值：是否成功
 * 函数说明：根据指定源文件ID判断是否有待办信息
 */
this.workService.isHaveToDoWork(docType, srcFileId);

/**
 * 入参： docType: String 文档类型
 * 入参：srcFileId: Object 源文件ID
 * 入参：flowInstanceId: String 流程实例ID
 * 返回值：是否成功
 * 函数说明：根据指定流程判断是否有待办信息
 */
this.workService.isHaveToDoWork(docType, srcFileId, flowInstanceId);

/**
 * 入参： docType: String 文档类型
 * 入参：srcFileId: Object 源文件ID
 * 返回值：是否成功
 * 函数说明：删除指定源文件ID已办信息
 */
this.workService.deleteDoneWork(docType, srcFileId);

/**
 * 入参： docType: String 文档类型
 * 入参：srcFileId: Object 源文件ID
 * 入参：flowInstanceId: String 流程实例ID
 * 返回值：是否成功
 * 函数说明：删除指定流程已办信息
 */
this.workService.deleteDoneWork(docType, srcFileId, flowInstanceId);

/**
 * 入参： docType: String 文档类型
 * 入参：srcFileId: Object 源文件ID
 * 入参： userIds: Object[] 已办人员ID
 * 返回值：是否成功
 * 函数说明：删除指定人员已办信息
 */
this.workService.deleteUserDoneWork(docType, srcFieldId, ...userIds);

/**
 * 入参： docType: String 文档类型
 * 入参：srcFileId: Object 源文件ID
 * 入参：flowInstanceId: String 流程实例ID
 * 入参： userIds: Object[] 待办人
 * 返回值：是否成功
 * 函数说明：删除指定流程中人员已办信息
 */
this.workService.deleteUserDoneWork(docType, srcFieldId, flowInstanceId, ...userIds);
```