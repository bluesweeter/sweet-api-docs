> `Notify模块`是对接平台统一通知服务模块接口，在代码中的关键字为 this.notifyService

## API说明  <!-- {docsify-ignore-all} -->
***

```js
/**
 * 入参：type：String 通知类型(MESSAGE：站内消息;WECOM:微信;EMAIL:邮件;SMS:短信，支持自定义扩展)
 * 入参：fileId：Object 源文件ID
 * 入参：docType：String 文档类型
 * 入参：subject：String 主题
 * 入参：summary：String 描述
 * 入参：content：String 内容
 * 入参：args：String 参数
 * 入参：userIds：Object[] 通知对象ID
 * 返回值：Boolean 是否成功
 * 函数说明：发送消息给指定人员
 */
this.notifyService.sendNotify(type, fileId, docType,subject, summary, content, args, ...userIds);

/**
 * 入参：type：String 通知类型(MESSAGE：站内消息;WECOM:微信;EMAIL:邮件;SMS:短信，支持自定义扩展)
 * 入参：fileId：Object 源文件ID
 * 入参：docType：String 文档类型
 * 入参：subject：String 主题
 * 入参：summary：String 描述
 * 入参：content：String 内容
 * 入参：args：String 参数
 * 入参：actorRule：String 规则
 * 入参：actorRuleArgs：String[] 规则参数 规则请参考：com.egrand.cloud.core.constants.ActorEnum
 * 返回值：Boolean 是否成功
 * 函数说明：按规则发送消息给指定人员
 */
this.notifyService.sendNotifyByRule(type, fileId, docType, subject,summary, content, args, actorRule, ...actorRuleArgs);
```