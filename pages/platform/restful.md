> 对现有`微服务`集成时，可以通过`RESTful`插件提供的`connectService`远程调用服务API，例如：

```java
let content = this.RESTful
    .connectService("egrand-cloud-platform-ram", "/user/list")
    .contentType("application/json")
    .header("Accept-Charset", "utf-8")
    .get()
    .getBody();
```

调用时会自动带上`自定义请求头`，无需手工添加。`RESTful`插件更多详情，请[参考](/pages/api/restful)