> `Ouinfo模块`是对接平台RAM服务的组织管理模块接口，在代码中的关键字为 this.ouInfoService

## API说明  <!-- {docsify-ignore-all} -->
***

```js
/**
 * 入参：id: Object 组织ID
 * 返回值：cn.hutool.json.JSONObject 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamOuinfo
 * 函数说明：根据ID获取组织信息
 */
this.ouInfoService.getById(id);

/**
 * 入参：code: String 组织编码
 * 返回值：cn.hutool.json.JSONObject 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamOuinfo
 * 函数说明：根据组织编码获取组织信息
 */
this.ouInfoService.getByCode(code);

/**
 * 入参：ids: Object[] 组织ID集合
 * 返回值：cn.hutool.json.JSONArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamOuinfo
 * 函数说明：根据ID集合获取组织信息集合
 */
this.ouInfoService.listByIds(...ids);

/**
 * 入参：codes: String[] 组织编码集合
 * 返回值：cn.hutool.json.JSONArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamOuinfo
 * 函数说明：根据组织编码集合获取组织信息集合
 */
this.ouInfoService.listByCodes(...codes);

/**
 * 入参：ids: Object[] 组织ID集合
 * 返回值：cn.hutool.json.JSONArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamOuinfo
 * 函数说明：根据ID加载组织信息，包含下级组织
 */
this.ouInfoService.children(...ids);
```