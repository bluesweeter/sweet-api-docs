> `Group模块`是对接平台RAM服务的岗位管理模块接口，在代码中的关键字为 this.groupService

## API说明  <!-- {docsify-ignore-all} -->
***

```js
/**
 * 入参：id: Object 岗位ID
 * 返回值：cn.hutool.json.JSONObject 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamGroup
 * 函数说明：根据ID获取岗位信息
 */
this.groupService.getById(id);

/**
 * 入参：userId: Object 用户ID
 * 返回值：cn.hutool.json.JSONArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamGroup
 * 函数说明：根据用户id获取岗位信息
 */
this.groupService.getUserGroups(userId);

/**
 * 入参：ouInfoScopeType: Int 组织范围 (0:全部;1:部门;2:单位;3:下级单位(下管一层);4:上级单位(纵向向上一层);)
 * 入参：groupCodes: String[] 岗位编码集合
 * 返回值：cn.hutool.json.JSONArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamUserInfoVO
 * 函数说明：根据岗位编码和组织范围获取岗位信息
 */
this.groupService.getGroupsByCodes(ouInfoScopeType, ...groupCodes);

/**
 * 入参：ouInfoScopeType: Int 组织范围 (0:全部;1:部门;2:单位;3:下级单位(下管一层);4:上级单位(纵向向上一层);)
 * 入参：groupNames: String[] 岗位名称集合
 * 返回值：cn.hutool.json.JSONArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamUserInfoVO
 * 函数说明：根据岗位名称和组织范围获取岗位信息
 */
this.groupService.getGroupsByNames(ouInfoScopeType, groupNames);
```