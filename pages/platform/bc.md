> `bc模块`是对接平台BC服务模块接口，在代码中的关键字为 this.bcService

## API说明  <!-- {docsify-ignore-all} -->
***

```js
/**
 * 入参：typeKeys: String[] 类型数组
 * 返回值：cn.hutool.json.JSONOArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginBCDictionaryType
 * 函数说明：根据类型获取多个字典列表信息(列表方式加载，包括子可选项）
 */
this.bcService.listDictionary(...typeKeys);

/**
 * 入参：typeKey: String 类型
 * 入参：dictionaryCode: String 可选项编码
 * 返回值：cn.hutool.json.JSONOArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginBCDictionaryType
 * 函数说明：获取指定类型下可选项编码的子可选项(列表方式加载，只包含子选项)
 */
this.bcService.listDictionaryByType(typeKey, dictionaryCode);

/**
 * 入参：typeKey: String 类型
 * 返回值：cn.hutool.json.JSONOArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginBCDictionaryTypeItemTree
 * 函数说明：根据类型获取可选项树(树方式加载，用children属性包括子选项)
 */
this.bcService.listDictionaryTree(typeKey)

/**
 * 入参：cfgKey: String 编码
 * 返回值：cn.hutool.json.JSONObject 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginBCSystemConfig
 * 函数说明：根据参数编码获取参数配置
 */
this.bcService.getByCfgKey(cfgKey);

```