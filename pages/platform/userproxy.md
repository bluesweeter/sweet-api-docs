> `UserProxy模块`是对接平台用户授权服务模块接口，在代码中的关键字为 this.userProxyService

## API说明  <!-- {docsify-ignore-all} -->
***

```js
/**
 * 入参：docType : String 文档类型
 * 入参：userNames : String[] 用户名集合
 * 返回值： cn.hutool.json.JSONOArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamUserProxy
 * 函数说明：根据DocType和用户名获取用户授权
 */
this.userProxyService.listByUserName(docType, ...userNames);

/**
 * 入参：docType : String 文档类型
 * 入参：userNames : Object[] 用户名ID
 * 返回值： cn.hutool.json.JSONOArray 具体字段请参考：com.egrand.cloud.plugin.aop.sidecar.dto.PluginRamUserProxy
 * 函数说明：根据DocType和用户Id获取用户授权
 */
this.userProxyService.listByUserId(docType, ...userIds);
```