
* 平台集成
  * [上下文](/pages/platform/)
  * [RESTful调用](/pages/platform/restful.md)
  * [用户管理](/pages/platform/user.md)
  * [组织管理](/pages/platform/ouinfo.md)
  * [岗位管理](/pages/platform/group.md)
  * [业务配置](/pages/platform/bc.md)
  * [统一待办](/pages/platform/work.md)
  * [统一通知](/pages/platform/notify.md)
  * [用户授权](/pages/platform/userproxy.md)
  * [附件管理](/pages/platform/atm.md)
  * [流程服务](/pages/platform/workflow.md)

