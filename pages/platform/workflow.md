> `Workflow模块`，开发者可以通过在流程服务中配置监听事件或脚本任务方式调用SweetApi的接口，用于执行特定业务逻辑；

## 任务监听事件  <!-- {docsify-ignore-all} -->
***

![任务监听事件](../../_media/platform/workflowTask.png ':size=60%')

```json
"sweetApi": [{
    "eventType": "TASK_CREATED",
    "method": "post",
    "path": "/demo1/workflow",
    "context": {
        "name": "zhouzhihu",
        "sex": "男"
    }
}]
```

如上，在`用户任务创建`时调用`POST`接口`/demo1/workflow`，并传递自定义参数`name`和`sex`。

通过配置sweetApi来设置需要调用的API接口，包含属性如下：

* eventType： 事件类型，包括：TASK_ASSIGNED(任务分配)、TASK_CREATED(任务创建)、TASK_COMPLETED(任务完成)；

* method：请求方法，请根据sweetApi接口的实际请求方法设置；

* path：sweetApi的接口路径；

* context: 需要传递的参数，JSON类型，在sweetApi接口中通过`this.[变量名称]`访问；

除了以上context自定义提供参数外，同时也提供了以下内置参数，在sweetApi接口中通过`this.[变量名称]`访问：

* formData：`Map<String, Object>`类型，可以直接通过`this.formData.[变量名称]`获取流程中定义的formData参数；

* userTaskId：`字符串类型`，用户任务ID；

* userTaskName：`字符串类型`，用户任务名称；

* workerList：`字符串Array类型`，获取当前用户任务待办人，通过`JSON.parse()`方法转为`JSON`后处理，数组对象元素包括：id(唯一id)、code(编码)、name(中文名称)、workType(处理人员的类型)；

下面是接口书写时示例：

![sweetApi](../../_media/platform/sweetApiWorkflow.png ':size=60%')

## 脚本任务  <!-- {docsify-ignore-all} -->
***

![脚本任务](../../_media/platform/workflowTaskScript.png ':size=60%')

```js
pluginESBService.execute("POST", "/demo1/workflow",{
    "formData":formData
});
```

如上，通过调用`pluginESBService`方法`execute`来调用sweetApi的`POST`接口`/demo1/workflow`，并传递`formData`参数。

execute参数如下：

* 第一个参数：请求方法，请根据sweetApi接口的实际请求方法设置；

* 第二个参数：sweetApi的接口路径；

* 第三个参数：JSON类型，在sweetApi接口中通过`this.[变量名称]`访问；

其中第三个`formData`值，是获取流程内置参数，还包含如下参数：

![内置变量](../../_media/platform/taskScriptVar.png ':size=60%')

