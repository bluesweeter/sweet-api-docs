## 脚本调用Java <!-- {docsify-ignore-all} -->
***

> 可以通过Java.type("类名全路径")方式，引入Java类使用，引入后使用的方法跟Java中一样。

```js
let result = {};

// 引入java.text.SimpleDateFormat
var SimpleDateFormat = Java.type("java.text.SimpleDateFormat");
// 引入java.util.Locale
var Locale = Java.type("java.util.Locale");
// 创建一个SimpleDateFormat对象
let date = new SimpleDateFormat("EEE, d MMM yyyy hh:mm:ss Z", Locale.ENGLISH).format(new Date()) + " GMT"
// 添加到返回结果中
result.date = date;

// 引入 java.util.ArrayList
var ArrayListType = Java.type("java.util.ArrayList");
// 创建一个ArrayList对象
var arrayList = new ArrayListType();
// 调用add方法添加元素
arrayList.add("a");
arrayList.add("b");
// 日志输出数组值
this.log.info(arrayList.toString());
// 添加到返回结果
result.arrayList = arrayList;

// 输出结果，响应给调用者
return result;
```

> 输出结果

![执行结果](../../_media/script/script2java.png)

