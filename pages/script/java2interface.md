## Java调用接口 <!-- {docsify-ignore-all} -->
***

> 提供`Feign API`接口方式给第三方调用。

* `Feign`客户端声明

```java
@FeignClient(name = "${egrand.service.platform.esb.name:egrand-esb}",
        url = "${egrand.service.platform.esb.url}",
        path = "/api",
        contextId = "api-service-client",
        qualifier = "apiServiceClient",
        fallback = ApiServiceClientFallbackFactory.class, decode404 = true)
public interface ApiServiceClient {

    @ApiOperation(value = "执行API", notes = "执行API")
    @PostMapping("/execute")
    ResultBody execute(@RequestBody ApiExecuteDTO apiExecuteDTO);
}
```

* 调用时需要的`执行参数`

```java
@ApiModelProperty(value = "请求方式")
private String method;

@ApiModelProperty(value = "路径")
private String path;

@ApiModelProperty(value = "参数")
private Map<String, Object> context;
```