## 接口互调 <!-- {docsify-ignore-all} -->
***

> 在`Sweet-API`脚本书写时，需要调用其他的`接口`时。在脚本书写过程中，可以通过提供的`this.sweet.execute()`调用其他的接口。

```js
/**
 * 执行SweetAPI中的接口,原始内容，不包含code以及message信息
 * @param {string} method 请求方法
 * @param {string} path 请求路径，可以通过右键点击需要调用的接口选择【复制相对路径】获得
 * @param {Map<String, Object>} context 运行参数
 * @returns {<T>}
 */
this.sweet.execute("GET", "/api/test", {id: 265});
```