## 自定义状态码 <!-- {docsify-ignore-all} -->

目前内置了四种状态码，分别为 `执行成功(0)`，`参数验证失败(-404)`，`系统异常(-1)`，以及`自定义异常(-9)`；

## 响应Json结构

```json
{
    "code": 0, // 状态码
    "message": "success", // 状态说明
    "path": null, // 请求路径
    "data": ...,  // 返回数据内容
    "extra": null, // 扩展数据
    "timestamp": 1683561131518 //服务器时间
}
```

## 自定义结构配置

编写`Java`代码如下：

```java
/**
 * 默认结果封装实现
 *
 */
public class DefaultResultProvider implements ResultProvider {

	private final String responseScript;

	public DefaultResultProvider() {
		this.responseScript = null;
	}

	public DefaultResultProvider(String responseScript) {
		this.responseScript = responseScript;
	}

	@Override
	public Object buildResult(RequestEntity requestEntity, int code, String message, Object data) {
		return new ResultBody<>(code, data, message);
	}
}
```