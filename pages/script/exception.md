## 统一异常处理 <!-- {docsify-ignore-all} -->

> 通过覆盖ResultProvider的buildException实现全局异常处理。

```java
@Component
public class MyResultProvider implements ResultProvider {

	@Override
	public Object buildException(RequestEntity requestEntity, Throwable throwable) {
		return buildResult(requestEntity, 500, "系统内部出现错误");
	}
}
```