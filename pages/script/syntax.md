## 脚本引擎
***

`Sweet-API`脚本默认采用`GraalJSScriptEngine`引擎运行脚本，支持JavaScript的ES6语法。

> 书写脚本时，要求对Java和ES6具有一定的语法了解。

## ES6语法

`ES6`是JavaScript的一种新的版本号6的标准，以下列出了常用的技巧，帮助我们更好的书写脚本。想了解更多请下载[《深入浅出ES6》](https://www.wenjiangs.com/wp-content/uploads/2022/02/ES6-in-depth.zip ':target=_blank')。

## 变量声明

`ES6`为解决`var`声明变量的弊端，提供`let`和`const`：

`let`是一个更完美的`var`，它有着更好的作用域规则；

`const`与`let`声明的变量类似，声明的变量只可以在声明时赋值，不可随意修改，否则会导致SyntaxError；

```js
// 声明变量
let t = "Hello";

// 只在for循环块作用域下有效
for (let i = 0; i < messages.length; i++) {
    ...
}

 // const正确写法
const MAX_CAT_SIZE_KG = 3000;
// 用 const 声明变量后必须要赋值，否则也抛出语法错误
const theFairest;
// 语法错误（SyntaxError）
MAX_CAT_SIZE_KG = 5000;
// 虽然换了一种方式，但仍然会导致语法错误
MAX_CAT_SIZE_KG++;
```

## 箭头函数

这种语法是对匿名函数的一种简写 -  箭头函数

```js
let fn = ()=>{
    console.log(11);
}
fn();
```

当只有一个形参的时候，小括号可以省略

```js
let fn = a => {
    console.log(parseInt(a));
}
fn('5');
```

当大括号中只有一行代码的时候，大括号是可以省略的，如果大括号中这一行代码中有return，连return关键字一起省略

```js
let fn = a => console.log(parseInt(a));
fn('6')
 
var fn = function(a) {
    return parseInt(a) 
}
let fn = a => parseInt(a)
var res = fn('7')
console.log(res);
```

## ...运算符

`...`可以将一个数组或对象展开成多个值或多个键值对，也可以将多个值合并为一个数组
...对象/...数组

```js
var pbj = {
    name: '唐伯虎',
    sex: '男'
}
var obj = {
    // 将pbj对象，展开成多个键值对放在这里
    ...pbj,
    wife: {
        name: '如花',
        age: 21,
        sex: '女'
    }
}
console.log(obj);
```

## 模板字符串

`ES6`引入了一种新型的字符串字面量语法，我们称之为`模板字符串`（template strings），使用反撇号字符。为`JavaScript`提供了简单的字符串插值功能。

```js
// 变量插值
let name = "张三";
let msg = "有新消息"；
let content = `${name}您好，您${ms}!`

// 模板字符串中所有的换行、空格、新行、缩进，都会原样输出在生成的字符串中。
let maxPenalty = 5;
let html = `
 <h1>小心！>/h1>
 <p>未经授权打冰球可能受罚
 将近${maxPenalty}分钟。</p>
`;
```

## 解构

解构赋值允许你使用类似数组或对象字面量的语法将数组和对象的属性赋给各种变量。这种赋值语法极度简洁，同时还比传统的属性访问方法更为清晰。

> 数组与迭代器的解构

```js
// 声明 `v1, v2, v3` 的变量，并赋予数组中相应元素项`1,3,4`的值
let [ v1, v2, v3 ] = [1,3,4,5,6,7];

// 跳过被解构数组中的"foo","bar"元素，将"baz"赋值给v3，并声明v3变量
let [,,v3] = ["foo", "bar", "baz"];

// 将被结构数组中元素1赋值给v1变量，后面其它的组成一个新的数组赋值给v2变量；
let [v1, ...v2] = [1, 2, 3, 4];

// 将1赋值给v1，将2赋值给v2，将3赋值给v3
var [v1, [[v2], v3]] = [1, [[2], 3]];

```

> 对象的解构

```js
// 将对象`obj`的`name值`赋值给变量`nameA`,将`age值`赋值给变量`ageA`
let obj = { name: "Bender", age: 12 };
let { name: nameA, age: ageA } = obj;

// 当属性名与变量名一致时，可以通过一种实用的句法简写
// 将对象`obj`的`name值`赋值给变量`name`,将`age值`赋值给变量`age`
let {name, age} = obj;

```

## for-of 循环

> 与 forEach()不同的是，它可以正确响应 break、continue 和 return 语句

```js
// 数组轮询
let a = [1,2,3];
for (let b of a) {
    this.log.info(b + "");
}

// 从数据库中查询数据
let ouinfos = db.camel().table("egd_org_ouinfo").select();
// 轮询结果对象
for (let ouinfo of ouinfos) {
    this.log.info(ouinfo.ouName);
}

```

## null和undefined的变量赋初值的方法

```js
let str1 = null
let res = || '张三'
console.log("res=",res)   //结果：张三
```

```js
let str1 = undefined
let res = str1 || '张三'
console.log("res=",res)   //结果：张三
```

> 但是当属性的值如果为`空字符串`或`false`或`0`，默认值也会生效。这也就意味着当属性值为`null、undefined、false、0`这四种情况时默认值都会生效，避免这种情况ES2020 引入了一个新的 null判断运算符`??`

只有运算符`??`左侧的值为`null`或`undefined`时，才会返回右侧的值。

```js
const p1 = response.data.show ?? true
```

> 上面代码中，默认值true只有在response.data.show的值为null或undefined时，才会生效然后赋值给p1

## 链判断运算符

用于读取对象属性时判断是否为null或者undefined，当我们需要调用一个对象的某个属性但是我们却不确定这个属性是否存在时，如果直接读取的话可能就会报错，所以我们需要在调用之前先判断这个属性是否存在。

```js
// 错误的写法，可能会报错
const  firstName = message.body.user.firstName;

// 正确的写法
const firstName = (message
  && message.body
  && message.body.user
  && message.body.user.firstName) || 'default';
```

这样读取一个属性显得非常麻烦，ES2020引入了链判断运算符 `?.` 来简化这个操作：

```js
const firstName = message?.body?.user?.firstName || 'default';
```

上面代码使用了?.运算符，直接在链式调用的时候判断，左侧的对象是否为null或undefined。如果是的，就不再往下运算，而是返回undefined，然后将默认值default赋值给变量。


链判断运算符还可以在调用方法时判断该方法是否存在：

```js
iterator.return?.()
```

## Java与脚本交互

> Java对象传递到脚本中，使用方式与在Java中一致

```js
// ouInfoList接收的值是查询出来的List<Map<String, Object>对象
let ouInfoList = db.camel().table("egd_org_ouinfo").select();
// 这样ouInfoList就具有了类型List<Map<String, Object>的所有方法
// 可以调用size()方法获取查询数量
this.log.info(ouInfoList.size() + "");
```

```js
// 引入Java对象
let Collectors = Java.type("java.util.stream.Collectors");
// 调用stream，将id组成List对象
let idList = ouInfoList.stream().map(ouinfo =>{
    return ouinfo.id
}).collect(Collectors.toList());
// for...of 轮询List对象
for (let id of idList) {
    this.log.info(id + "");
}
```