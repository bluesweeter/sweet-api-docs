> `RESTful模块`是插件模块，需要在【插件管理】中安装插件才能使用。

## API说明 <!-- {docsify-ignore-all} -->
***

```js
/**
 * 入参：url : String 地址URL
 * 返回值：this
 * 函数说明：创建连接
 */
this.RESTful.connect(url);

/**
 * 入参：key : String 数据源中RESTful对应的key值，通过【右键】菜单【复制KEY】即可获得
 * 入参：url : String 地址URL
 * 返回值：this
 * 函数说明：创建连接
 */
this.RESTful.connect(key, url);

/**
 * 入参：serviceName : String 服务名
 * 入参：path : String 路径
 * 返回值：this
 * 函数说明：本地服务调用
 */
this.RESTful.connectService(serviceName, path)

/**
 * 入参：method : String 请求方法 GET|HEAD|POST|PUT|PATCH|DELETE|OPTIONS|TRACE
 * 返回值：this
 * 函数说明：设置请求方法，默认GET
 */
this.RESTful.method(method);

/**
 * 入参：contentType : String Content-Type值
 * 返回值：this
 * 函数说明：设置`ContentType`
 */
this.RESTful.contentType(contentType);

/**
 * 入参：body : String RequestBody
 * 返回值：this
 * 函数说明：设置`RequestBody`
 */
this.RESTful.body(body);

/**
 * 入参：key : String header名
 * 入参：value : String header值
 * 返回值：this
 * 函数说明：设置header
 */
this.RESTful.header(key, value);

/**
 * 入参：values : Map<String, Object> header值集合
 * 返回值：this
 * 函数说明：批量设置header
 */
this.RESTful.header(Map<String, Object> values);

/**
 * 入参：key : String 参数名
 * 入参：values : ...Object 参数值
 * 返回值：this
 * 函数说明：设置URL参数
 */
this.RESTful.param(key, ...values);

/**
 * 入参：values : Map<String, Object> 参数值
 * 返回值：this
 * 函数说明：批量设置URL参数
 */
this.RESTful.param(values);

/**
 * 入参：key : String 参数名
 * 入参：values : ...Object 参数值
 * 返回值：this
 * 函数说明：设置form参数
 */
this.RESTful.data(key, ...values);

/**
 * 入参：values : Map<String, Object> 参数值
 * 返回值：this
 * 函数说明：批量设置form参数
 */
this.RESTful.data(values);

/**
 * 返回值：this
 * 函数说明：设置返回值为`byte[]`
 */
this.RESTful.expectBytes();

/**
 * 返回值：this
 * 函数说明：设置返回值为`JSONObject`
 */
this.RESTful.expectJson();

/**
 * 返回值：ResponseEntity<?>
 * 函数说明："发送`GET`请求"，用于设置参数后调用
 */
this.RESTful.get();

/**
 * 返回值：ResponseEntity<?>
 * 函数说明："发送`POST`请求"，用于设置参数后调用
 */
this.RESTful.post();

/**
 * 返回值：ResponseEntity<?>
 * 函数说明："发送`PUT`请求"，用于设置参数后调用
 */
this.RESTful.put();

/**
 * 返回值：ResponseEntity<?>
 * 函数说明："发送`DELETE`请求"，用于设置参数后调用
 */
this.RESTful.delete();

/**
 * 返回值：ResponseEntity<?>
 * 函数说明："发送`HEAD`请求"，用于设置参数后调用
 */
this.RESTful.head();

/**
 * 返回值：ResponseEntity<?>
 * 函数说明："发送`OPTIONS`请求"，用于设置参数后调用
 */
this.RESTful.options();

/**
 * 返回值：ResponseEntity<?>
 * 函数说明："发送`TRACE`请求"，用于设置参数后调用
 */
this.RESTful.trace();
```