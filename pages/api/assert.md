## Assert断言 <!-- {docsify-ignore-all} -->
***

> `assert模块`是默认引入的模块，所以无需引入

`AssertModule`用于判断是否满足对应条件，如果不满足，则终止运行并抛出对应的异常信息。

## API说明
***

```js
/**
 * 判断一个对象是否为空，不为空则抛出异常
 * @param {Object} object 对象
 * @param {String} message 异常消息
 */
this.assert.isNull(object, message);

/**
 * 判断一个对象是否为空，不为空则抛出异常
 * @param {Object} object 对象
 * @param {Supplier<String>} messageSupplier 提供回调函数，获取异常消息
 */
this.assert.isNull(object, messageSupplier);

/**
 * 判断一个对象不为空，为空则抛出异常
 * @param {Object} object 对象
 * @param {String} message 异常消息
 */
this.assert.notNull(object, message);

/**
 * 判断一个对象不为空，为空则抛出异常
 * @param {Object} object 对象
 * @param {Supplier<String>} messageSupplier 提供回调函数，获取异常消息
 */
this.assert.notNull(object, messageSupplier);

/**
 * 判断字符串是否有长度，没有则抛出异常
 * @param {String} text 字符串
 * @param {String} message 异常消息
 */
this.assert.hasLength(text, message);

/**
 * 判断字符串是否有长度，没有则抛出异常
 * @param {String} text 字符串
 * @param {Supplier<String>} messageSupplier 提供回调函数，获取异常消息
 */
this.assert.hasLength(text, messageSupplier);

/**
 * 判断字符串是否有长度，并包含字符串，没有则抛出异常
 * @param {String} text 字符串
 * @param {String} message 异常消息
 */
this.assert.hasText(text, message);

/**
 * 判断字符串是否有长度，并包含字符串，没有则抛出异常
 * @param {String} text 字符串
 * @param {Supplier<String>} messageSupplier 提供回调函数，获取异常消息
 */
this.assert.hasText(text, messageSupplier);

/**
 * 判断数组对象不为空，为空则抛出异常
 * @param {Object[]} array 数组
 * @param {String} message 异常消息
 */
this.assert.notEmpty(array, message);

/**
 * 判断数组对象不为空，为空则抛出异常
 * @param {Object[]} array 数组
 * @param {Supplier<String>} messageSupplier 提供回调函数，获取异常消息
 */
this.assert.notEmpty(array, messageSupplier);

/**
 * 判断Collection对象不为空，为空则抛出异常
 * @param {Collection<?>} collection Collection
 * @param {String} message 异常消息
 */
this.assert.notEmpty(collection, message);

/**
 * 判断Collection对象不为空，为空则抛出异常
 * @param {Collection<?>} collection Collection
 * @param {Supplier<String>} messageSupplier 提供回调函数，获取异常消息
 */
this.assert.notEmpty(collection, messageSupplier);

/**
 * 判断Map对象不为空，为空则抛出异常
 * @param {Map<?, ?>} map Map对象
 * @param {String} message 异常消息
 */
this.assert.notEmpty(map, message);

/**
 * 判断Map对象不为空，为空则抛出异常
 * @param {Map<?, ?>} map Map对象
 * @param {Supplier<String>} messageSupplier 提供回调函数，获取异常消息
 */
this.assert.notEmpty(map, messageSupplier);

/**
 * 判断表达式结果为true，不为true则抛出异常
 * @param {boolean} expression 布尔表达式
 * @param {String} message 异常消息
 */
this.assert.isTrue(expression, message);

/**
 * 判断表达式结果为true，不为true则抛出异常
 * @param {boolean} expression 布尔表达式
 * @param {Supplier<String>} messageSupplier 提供回调函数，获取异常消息
 */
this.assert.isTrue(expression, messageSupplier);

/**
 * 正则判断，判断输入的内容符合正则，不符合则抛出异常
 * @param {String} expression 正则表达式
 * @param {String} input 输入值
 * @param {String} message 异常消息
 */
this.assert.regex(regex, input, message);

/**
 * 正则判断，判断输入的内容符合正则，不符合则抛出异常
 * @param {String} expression 正则表达式
 * @param {String} input 输入值
 * @param {Supplier<String>} messageSupplier 提供回调函数，获取异常消息
 */
this.assert.regex(regex, input, messageSupplier);
```