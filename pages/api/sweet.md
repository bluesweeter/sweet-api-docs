## Sweet <!-- {docsify-ignore-all} -->
***
> `sweet模块`是默认引入的模块，所以无需引入

SweetModule提供接口间相互调用的execute方法，主要用于公共接口抽离
被调用接口语法与普通接口一致，只不过没有request、response、header等前端提供的参数，
需要调用接口通过context传递过来

## API说明
***

```js

/**
 * 执行SweetAPI中的接口,原始内容，不包含code以及message信息
 * @param {string} method 请求方法
 * @param {string} path 请求路径，可以通过右键点击需要调用的接口选择【复制相对路径】获得
 * @returns {<T>}
 */
this.sweet.execute("GET", "/api/test");

/**
 * 执行SweetAPI中的接口,原始内容，不包含code以及message信息
 * @param {string} method 请求方法
 * @param {string} path 请求路径，可以通过右键点击需要调用的接口选择【复制相对路径】获得
 * @param {Map<String, Object>} context 运行参数
 * @returns {<T>}
 */
this.sweet.execute("GET", "/api/test", {id: 265});
```