> `RabbitMQ插件`是插件模块，需要在【插件管理】中安装插件才能使用。

## API说明  <!-- {docsify-ignore-all} -->
***

```js

/**
 * 切换数据源
 * @param {string} mqKey MQ数据源KEY
 * @returns {<BoundMQModule>}
 */
mq(mqKey)

/**
 * 发送消息
 * @param {string} queueName 队列名称
 * @param {Object} msg 消息内容
 */
send(queueName, msg);

/**
 * 发送消息
 * @param {string} queueName 队列名称
 * @param {Object} msg 消息内容
 * @param {Map<String, Object>} headers 头信息
 */
send(queueName, msg, headers);

/**
 * 发送消息
 * @param {string} queueName 队列名称
 * @param {Object} msg 消息内容
 * @param {function} mqp2eCallbackSuccessFunction MQ生成者到交换机发送成功后回调接口
 * @param {function} mqp2eCallbackFailureFunction MQ生成者到交换机发送失败后回调接口
 */
send(String queueName, Object msg, 
    Function<Map<String, Object>, Boolean> mqp2eCallbackSuccessFunction, 
    Function<Map<String, Object>, Boolean> mqp2eCallbackFailureFunction);

/**
 * 发送消息
 * @param {string} queueName 队列名称
 * @param {Object} msg 消息内容
 * @param {Map<String, Object>} headers 头信息
 * @param {function} mqp2eCallbackSuccessFunction MQ生成者到交换机发送成功后回调接口
 * @param {function} mqp2eCallbackFailureFunction MQ生成者到交换机发送失败后回调接口
 */
send(String queueName, Object msg, headers, 
    Function<Map<String, Object>, Boolean> mqp2eCallbackSuccessFunction, 
    Function<Map<String, Object>, Boolean> mqp2eCallbackFailureFunction);

/**
 * 发送消息
 * @param {string} queueName 队列名称
 * @param {Object} msg 消息内容
 * @param {function} mqp2eCallbackSuccessFunction MQ生成者到交换机发送成功后回调接口
 * @param {function} mqp2eCallbackFailureFunction MQ生成者到交换机发送失败后回调接口
 * @param {function} mqe2qCallbackFunction MQ从交换机到队列失败后的回调函数
 */
send(String queueName, Object msg,
    Function<Map<String, Object>, Boolean> mqp2eCallbackSuccessFunction,
    Function<Map<String, Object>, Boolean> mqp2eCallbackFailureFunction,
    Function<Map<String, Object>, Boolean> mqe2qCallbackFunction);

/**
 * 发送消息
 * @param {string} queueName 队列名称
 * @param {Object} msg 消息内容
 * @param {Map<String, Object>} headers 头信息
 * @param {function} mqp2eCallbackSuccessFunction MQ生成者到交换机发送成功后回调接口
 * @param {function} mqp2eCallbackFailureFunction MQ生成者到交换机发送失败后回调接口
 * @param {function} mqe2qCallbackFunction MQ从交换机到队列失败后的回调函数
 */
send(String queueName, Object msg, headers, 
    Function<Map<String, Object>, Boolean> mqp2eCallbackSuccessFunction,
    Function<Map<String, Object>, Boolean> mqp2eCallbackFailureFunction,
    Function<Map<String, Object>, Boolean> mqe2qCallbackFunction);

```