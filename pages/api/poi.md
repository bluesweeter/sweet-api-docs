> `POI插件`是插件模块，需要在【插件管理】中安装插件才能使用。

## 示例  <!-- {docsify-ignore-all} -->

* 模板制作

```text
模板制作分为以下三种情况：
1、普通数据渲染，采用`{{}}`方式配置；
2、Html渲染采用的书签方式配置；
3、插入Word附件采用书签方式配置；
```
![word模板](../../_media/api/poi.png ":size=70%")

* 脚本

```js
let html = "<p>糠酸莫米松乳膏剂：<br />（1）选择合适的基质，解决主药的溶解、分散、乳化均匀等是拟要解决的技术问题。<br />①制得乳膏的稳定性是必须考察的内容，以保证制剂的质量稳定。<br />②乳膏制备工艺扩大生产可行性是本研究可否用于大量工业生产的关键。<br />（2）选定处方中合理的具有代表性的成分作为质控标准，建立定量、定性鉴别的方法是该乳膏的质量保证，也是本项目拟解决的关键技术之一。</p>\n" +
                "<p><img src=\"http://10.1.10.184:31818/osp/20240116/A91D599D5F3E410B807AA22FB5B66954/12bb76791112b5b2fb436e636647e5f6/a1746b51-c0c7-450a-80ed-48ce09288c1e.jpg\" alt=\"图片\" /></p>\n" +
                "<table style=\"border-collapse: collapse; width: 731px; height: 45px;\" border=\"1\">\n" +
                "<tbody>\n" +
                "<tr>\n" +
                "<td style=\"width: 364px;\">测试</td>\n" +
                "<td style=\"width: 364px;\">测试1</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td style=\"width: 364px;\">&nbsp;</td>\n" +
                "<td style=\"width: 364px;\">&nbsp;</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td style=\"width: 364px;\">&nbsp;</td>\n" +
                "<td style=\"width: 364px;\">&nbsp;</td>\n" +
                "</tr>\n" +
                "</tbody>\n" +
                "</table>\n" +
                "<p>糠酸莫米松凝胶剂：<br />（1）凝胶剂基质的制备<br />凝胶剂是由药物和凝胶基质组成的，凝胶基质是药物的载体，对于凝胶剂的质量产生较大的影响，因此，选择好的凝胶基质是很重要的。<br />根据已生产糠酸莫米松凝胶剂厂家的原辅料可知，该产品凝胶基质选择卡波姆，其关键步骤在于pH的调节。<br />（2）选择合适的基质后，药物与基质混合制剂成型亦是拟要解决的技术问题。<br />①制得凝胶剂的稳定性是必须考察的内容，以保证制剂的质量稳定。<br />②凝胶剂制备工艺扩大生产可行性是本研究可否用于大量工业生产的关键。<br />（3）选定处方中合理的具有代表性的成分作为质控标准，建立定量、定性鉴别的方法是该凝胶剂的质量保证。也是本项目拟解决的关键技术之一。</p>";
let html2 = "<p><img src=\"http://10.1.10.184:31818/osp/20240116/A91D599D5F3E410B807AA22FB5B66954/12bb76791112b5b2fb436e636647e5f6/a1746b51-c0c7-450a-80ed-48ce09288c1e.jpg\" alt=\"图片\" /></p>";
let data = {
    name: "测试填充",
    money: "121.1万元",
    a: "□值1",
    b: "☑值2",
    xmlb: ["产品二次开发", "新适应症"],
    xmlbqt: ["其他"],
    xmlbqtms: "测试",
    xcplb: ["药品", "中药饮片"],
    jdap: [
        {
            yjnr: "研究1\n研究2\n122112323",
            yjwcsj: "2021年2月 至 2021年3月",
        },{
            yjnr: "研究2",
            yjwcsj: "2021年4月 至 2021年6月",
        }
    ],
    jszb: [
        {
            name: "经济指标",
            content: "经济指标1"
        },
        {
            name: "经济指标",
            content: "经济指标2"
        },
        {
            name: "经济指标",
            content: "经济指标3"
        },
        {
            name: "其他指标",
            content: "其他指标1"
        },
        {
            name: "其他指标",
            content: "其他指标2"
        },
        {
            name: "测试",
            content: "测试"
        },
    ],
    ndzcys: "5",
    html: html,
    html2: html2,
    atm: ["c:\\4.docx"],
    atm1: ["c:\\智慧党建系统-政务云资源申请测算报告.docx"]
};

return word.builder()
    .tpl("c:\\lxjys.docx")
    .data(data)
    .bindBlankRenderPolicy("hzdw", (context) => {
        context.getRun().setText("123333", 0);
    })
    // html渲染
    .bindHtmlRenderPolicy("html", "html2")
    // 附件渲染
    .bindAtmRenderPolicy("atm", "atm1")
    // 复选框渲染
    .bindLoopCheckBoxRenderPolicy("xmlb", "xmlbqt", "xcplb")
    // 行循环
    .bindLoopRowTableRenderPolicy("jdap")
    // 行循环合并列
    .bindLoopRowTableMergeRowRenderPolicy("jszb", true, "name", 1)
    // 删除行渲染
    .bindRemoveRowTableRenderPolicy("ndzcys")
    .build("c:\\测试.docx", "测试水印").toPdf();
```

## word api说明

> 安装完插件后，就可以使用`word`关键字对象进行word文档相关操作。
***

```js

/**
 * 创建一个word操作对象
 * @returns {<WordModule>}
 */
builder();

/**
 * 设置word模板路径
 * @param {string} tplPath 模板路径
 * @returns {<WordModule>}
 */
tpl(tplPath);

/**
 * 设置模板数据
 * @param {Map<String, Object>} data 模板数据
 * @returns {<WordModule>}
 */
data(data);

/**
 * 自定义渲染
 * @param {string} tagName 标签名称
 * @param {Function<XWPFRunContext, Object>} callback 回调函数
 * @returns {<WordModule>}
 */
bindBlankRenderPolicy(tagName, callback);

/**
 * 带头表格循环行策略
 * @param {string[]} tagNames 标签名称集合
 * @returns {<WordModule>}
 */
bindLoopRowTableRenderPolicy(...tagNames);

/**
 * 不带头表格循环行策略
 * @param {string[]} tagNames 标签名称集合
 * @returns {<WordModule>}
 */
bindLoopRowTableNoHeaderRenderPolicy(...tagNames);

/**
 * 表格循环行策略(合并行)
 * @param {string} tagNames tagName 标签名称
 * @param {boolean} tagNames onSameLine false:带头模板/true:不带头模板
 * @param {string} tagNames mergeField 合并字段
 * @param {int} tagNames mergeCol 合并字段列序号(从1开始)
 * @returns {<WordModule>}
 */
bindLoopRowTableMergeRowRenderPolicy(tagName, onSameLine, mergeField, mergeCol);

/**
 * 复选框生成策略，根据标签值对比标签位置处后面的文本，决定时生成复选框还是选中复选框
 * @param {string[]} tagNames 标签名称集合
 * @returns {<WordModule>}
 */
bindLoopCheckBoxRenderPolicy(String... tagNames);

/**
 * HTML转换策略，将html富文本转换为word片段内容
 * @param {string[]} tagNames 标签名称集合
 * @returns {<WordModule>}
 */
bindHtmlRenderPolicy(String... tagNames);

/**
 * 插入word文档
 * @param {string[]} tagNames 标签名称集合
 * @returns {<WordModule>}
 */
bindAtmRenderPolicy(String... tagNames);

/**
 * 表格行删除策略，从标签所在行位置删除指定标签值的行数
 * @param {string[]} tagNames 标签名称集合
 * @returns {<WordModule>}
 */
bindRemoveRowTableRenderPolicy(String... tagNames);

/**
 * 生成word文档
 * @param {string} buildFilePath 文件导出路径
 * @returns {<WordModule>}
 */
build(buildFilePath);

/**
 * 生成带水印的word文档
 * @param {string} buildFilePath 文件导出路径
 * @param {string} waterText 水印
 * @returns {<WordModule>}
 */
build(buildFilePath, waterText);

/**
 * 下载word文档
 * @param {string} fileName 下载文件名
 * @returns {<ResponseEntity>}
 */
download(fileName);

/**
 * 生成pdf文档，在word同目录生成一个名称一样的pdf文档
 * @returns {string} pdf路径
 */
toPdf();

```

## wordUtil说明

> wordUtil是插件提供的帮助类，用于提供word的快捷操作，例如：html转word，添加水印等等

```js
/**
 * 在同目录下生成带水印的文件
 * @param {string} filePath 需要生成的水印文件
 * @param {string} destFileName 目标文件名
 * @param {string} water 水印文字
 * @throws Exception
 */
void addWaterMark(filePath, destFileName, water);

/**
 * html转word
 * @param {string} html html字符串
 * @param {string} filePath 保存的路径
 * @throws Exception
 */
void html2Word(html, filePath);

/**
 * 在同一目录下生成word相同文件名的pdf文件，word转pdf
 * @param {string} filePath word路径
 * @return {string} pdf生成路径
 * @throws IOException
 */
string word2Pdf(filePath);

/**
 * 在目标文档指定书签位置插入源文档，未指定书签则插入到目标文档的最后面
 * @param {string} desPath 目标文档路径
 * @param {string} srcPath 源文档路径
 * @param {string} bookmark 书签
 * @throws Exception
 */
void insertDocument(desPath, srcPath, bookmark);
```