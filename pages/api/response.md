## response引入 <!-- {docsify-ignore-all} -->
***
> response是默认引入的模块，所以无需引入

`response模块`为动态请求的响应提供操作API。

## API说明
***

```js
/**
 * 构建Json结果，直接返回该json，不会被包装处理
 * @param {Object} data 响应JSON数据
 * @returns {ResponseEntity} 响应实体
 */
response.json({
    success : true,
    message : '执行成功'
});

/**
 * 输出文本,直接返回该text，不会被包装处理
 * @param {string} data 响应文本内容数据
 * @returns {ResponseEntity} 响应实体
 */
response.text('ok');

/**
 * 重定向,重定向到该地址，内部利用HttpServletResponse的sendRedirect方法
 * @param {string} url 目标URL
 * @returns {NullValue} 空值
 */
response.redirect('/xxx/xx');

/**
 * 下载文件
 * @param {string} content 文件内容
 * @param {string} name 文件名称
 * @returns {ResponseEntity} 响应实体
 */
response.download('文件内容','test.txt');

/**
 * 下载文件
 * @param {Object} data 文件内容
 * @param {HttpHeaders} httpHeaders 自定义响应头 参考：org.springframework.http.HttpHeaders
 * @returns {ResponseEntity} 响应实体
 */
response.download(data, httpHeaders);

/**
 * 下载从Feign远程调用得到的文件
 * @param {Object} response Feign响应 参考：Feign.Response
 * @returns {ResponseEntity} 响应实体
 */
response.download(response);

/**
 * 输出图片
 * @param {Object} value 图片内容
 * @param {string} mime 图片类型，image/png,image/jpeg,image/gif
 * @returns {ResponseEntity} 响应实体
 */
response.image(value, mime);

/**
 * 添加Response Header
 * @param {string} key header名
 * @param {string} value header值
 * @returns {ResponseEntity} 响应实体
 */
response.addHeader(key, value);

/**
 * 入参：key:string
 * 入参：value:String
 * 返回值：无返回值
 * 函数说明：
 */
/**
 * 设置Response Header
 * @param {string} key header名
 * @param {string} value header值
 * @returns {ResponseEntity} 响应实体
 */
response.setHeader(key, value);

/**
 * 入参：key:string
 * 入参：value:String
 * 入参：options:Map cookie参数，可选
 * 返回值：无返回值
 * 函数说明：添加Cookie
 */
/**
 * 添加cookie
 * @param {string} name cookie名
 * @param {string} value cookie值
 * @returns {ResponseModule}
 */
response.addCookie('cookieKey','cookieValue');

/**
 * 添加cookie
 * @param {string} name Cookie名
 * @param {string} value Cookie值
 * @param {Map<String, Object>} options Cookie选项，如`path`、`httpOnly`、`domain`、`maxAge`"
 * @return
 */
response.addCookie('cookieKey','cookieValue',{
    path : '/',
    httpOnly : true,
    domain : 'ssssssss.org',
    maxAge : 3600
});

/**
 * 批量添加Cookie
 * @param {Map<String, String>} cookies Cookies
 * @returns {ResponseModule}
 */
response.addCookies({
    cookieKey1 : 'cookieValue1',
    cookieKey2 : 'cookieValue2',
});

/**
 * 批量添加cookie
 * @param {Map<String, String>} cookies Cookies
 * @param {Map<String, Object>} options Cookie选项，如`path`、`httpOnly`、`domain`、`maxAge`"
 * @returns {ResponseModule}
 */
response.addCookies({
    cookieKey1 : 'cookieValue1',
    cookieKey2 : 'cookieValue2',
},{
    path : '/',
    httpOnly : true,
    domain : 'ssssssss.org',
    maxAge : 3600
});



```