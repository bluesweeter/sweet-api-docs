## request引入 <!-- {docsify-ignore-all} -->
***
> request是默认引入的模块，所以无需引入

`request模块`为动态请求提供操作API。

## API说明
***

```js
/**
 * 退出请求
 * @param {string} code 编号(0：代表请求成功；-1：执行出现异常；-404：参数验证未通过；-9：自定义错误消息)
 * @param {string} params 消息（默认值为：success）
 * @param {string} data 响应数据
 * @returns {ExitValue} 退出值
 */
request.exit(code, msg, data);

/**
 * 获取上传的单个文件
 * @param {string} name 参数名称
 * @returns {MultipartFile} 附件
 */
request.getFile(name);

/**
 * 获取上传的文件集合
 * @param {string} name 参数名称
 * @returns {List<MultipartFile>} 附件集合
 */
request.getFiles(name);

/**
 * 获取提交的数组参数
 * @param {string} name 参数名称
 * @returns {List<String>} 参数值
 */
request.getValues(name);

/**
 * 获取请求的header数组
 * @param {string} name 请求头参数名称
 * @returns {List<String>} 请求头参数值
 */
request.getHeaders(name);

/**
 * 获取原生HttpServletRequest对象
 * @returns {HttpServletRequest} Http Request 对象
 */
request.get();

/**
 * 获取客户端IP
 * @returns {string} 客户端IP
 */
request.getClientIP();
```