> `redis模块`是插件模块，需要在【插件管理】中安装插件才能使用。

## API说明  <!-- {docsify-ignore-all} -->
***

```js
/**
 * 入参：key: String 键
 * 入参：value: Object 值
 * 返回值：Boolean 是否成功
 * 函数说明：设置redis键值
 */
redis.set(key, value);

/**
 * 入参：key: String 键
 * 返回值：Object 值
 * 函数说明：获取redis指定键的值
 */
redis.get(key);

/**
 * 
 * 入参：key: String 键
 * 入参：value: Object 值
 * 返回值：Boolean 是否成功
 * 函数说明：指定Redis主机写入键值，rs方法用于指定操作的Redis服务器，可以在【数据源】中进行配置。
 */
redis.rs("redis").set(key, value);

/**
 * 入参：key: String 键
 * 返回值：Object 值
 * 函数说明：获取指定redis服务器上的键值，rs方法用于指定操作的Redis服务器，可以在【数据源】中进行配置。
 */
redis.rs("redis").get(key);
```
