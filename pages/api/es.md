> `Elasticsearch模块`是插件模块，需要在【插件管理】中安装插件才能使用。

## API说明 <!-- {docsify-ignore-all} -->
***

```js
/**
 * 入参：indexName : String index名称
 * 入参：indexMapping : String Mapping脚本
 * 返回值：
 * 函数说明：创建IndexMapping
 */
this.es.createIndiceMapping(indexName, indexMapping);

/**
 * 入参：index String : index名称
 * 入参：pretty Boolean : 是否增加pretty
 * 返回值：
 * 函数说明：获取IndexMapping
 */
this.es.getIndexMapping(index, pretty);

/**
 * 入参：index String : index名称
 * 入参：indexType String : index类型
 * 返回值：List<IndexField> 字段列表
 * 函数说明：获取IndexMapping字段
 */
this.es.getIndexMappingFields(index, indexType);

/**
 * 入参：indexName : String index名称
 * 返回值：Boolean
 * 函数说明：判断是否存在index
 */
this.es.existIndice(indiceName);

/**
 * 入参：index String : index名称
 * 返回值：String
 * 函数说明：删除index
 */
this.es.dropIndice(index);

/**
 * 入参：indexName : String index名称
 * 入参：entity : Map<String,Object> 实体
 * 返回值：
 * 函数说明：增加记录
 */
this.es.addDocument(indexName, entity);

/**
 * 入参：indexName : String index名称
 * 入参：entity : List<Map<String, Object>> 实体列表
 * 返回值：
 * 函数说明：批量增加记录
 */
this.es.addDocuments(indexName, entityList);

/**
 * 入参：indexName : String index名称
 * 入参：id : Object ES自生成的id
 * 入参：entity : Map<String, Object> 实体列表
 * 返回值：
 * 函数说明：更新记录
 */
this.es.updateDocument(index, id, entity);

/**
 * 入参：indexName : String index名称
 * 入参：indexType : String index类型
 * 入参：id : Object ES自生成的id
 * 返回值：
 * 函数说明：删除记录
 */
this.es.deleteDocument(indexName, indexType, id);

/**
 * 入参：indexName : String index名称
 * 入参：ids : String[] ES自生成的id
 * 返回值：
 * 函数说明：批量删除记录
 */
this.es.deleteDocuments(indexName, ids);

/**
 * 入参：indexName : String index名称
 * 入参：documentId : String ES自生成的id
 * 返回值：
 * 函数说明：获取记录
 */
this.es.getDocument(indexName, documentId);

/**
 * 入参：indexName : String index名称
 * 入参：fieldName : String 字段名称
 * 入参：fieldValue : Object 字段值
 * 返回值：
 * 函数说明：按字段获取记录
 */
this.es.getDocumentByField(indexName, fieldName, fieldValue);

/**
 * 入参：indexName : String index名称
 * 入参：fieldName : String 字段名称
 * 入参：fieldValue : Object 字段值
 * 返回值：
 * 函数说明：按字段模糊查询获取记录
 */
this.es.getDocumentByFieldLike(indexName, fieldName, fieldValue);

/**
 * 入参：path : String 路径
 * 返回值：
 * 函数值：分页获取记录
 */
this.es.page(path);

/**
 * 入参：path : String 路径
 * 入参：ascs : String 升序字段，多个用逗号隔开
 * 入参：descs : String 降序字段，多个用逗号隔开
 * 入参：filters : String 搜索条件JSON字符串
 * 返回值：
 * 函数值：分页获取记录
 */
this.es.page(path, ascs, descs, filters);
```