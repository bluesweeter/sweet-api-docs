## 本地文件管理 <!-- {docsify-ignore-all} -->
***
> `sweetFile模块`是默认引入的模块，所以无需引入

SweetFileModule为Sweet-API提供服务器文件操作接口

* 提供虚拟的文件目录管理；

* 提供两种方式管理文件，默认采用第二种：

  1）对接文件管理微服务，不进行物理附件存储，对接方式采用egrand-cloud-plugin-sidecar-atm插件；

  2）本地文件管理，在ESB的本地目录管理相关文件；

## API说明
***

```js
/**
 * 上传文件
 * @param {MultipartFile} multipartFile 文件
 * @param {number} parentId 文件夹ID，通过在[文件管理]右键点击文件夹，选择"复制ID"获取
 * @param {string} key 文件在文件管理中的[关键字]
 * @returns {JSONObject} 上传文件JSON信息 参考com.egrand.cloud.esb.client.model.entity.File
 */
this.sweetFile.upload(multipartFile, parentId, key);

/**
 * 上传文件到指定目录
 * @param {MultipartFile} multipartFile 文件
 * @param {string} path 路径目录
 * @returns {String} 上传后路径
 */
this.sweetFile.upload(MultipartFile multipartFile,  path);

/**
 * 获取文件管理中指定【关键字】的文件完整路径
 * @param {string} key 文件在文件管理中的[关键字]
 * @returns {string} 文件路径
 */
this.sweetFile.getFilePath(key);

/**
 * 下载文件管理中指定【关键字】的文件
 * @param {string} key 文件在文件管理中的[关键字]
 * @returns {ResponseEntity} 响应Entity
 */
this.sweetFile.download(key);
```