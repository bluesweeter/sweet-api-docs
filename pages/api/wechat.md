> `Wechat模块`是插件模块，需要在【插件管理】中安装插件才能使用。

## API说明  <!-- {docsify-ignore-all} -->
***

```js
/**
 * 返回值：this
 * 函数说明：设置文本消息
 */
this.wechat.text();

/**
 * 返回值：this
 * 函数说明：设置图片消息
 */
this.wechat.image();

/**
 * 返回值：this
 * 函数说明：设置语音消息
 */
this.wechat.voice();

/**
 * 返回值：this
 * 函数说明：设置视频消息
 */
this.wechat.video();

/**
 * 返回值：this
 * 函数说明：设置图文消息
 */
this.wechat.news();

/**
 * 入参：String[] : 接收用户ID
 * 返回值：this
 * 函数说明：设置接收用户
 */
this.wechat.userId(...userId)

/**
 * 入参：String[] : partyId
 * 返回值：this
 * 函数说明：设置partyId
 */
this.wechat.partyId(...partyId)

/**
 * 入参：String[] : tagId
 * 返回值：this
 * 函数说明：设置tagId
 */
this.wechat.tagId(...tagId)

/**
 * 入参：content : String 文本消息内容
 * 返回值：this
 * 函数说明：设置文本消息内容
 */
this.wechat.content(content);

/**
 * 入参：title : String 视频消息主题
 * 返回值：this
 * 函数说明：设置视频消息主题
 */
this.wechat.title(title);

/**
 * 入参：description : String 视频消息描述
 * 返回值：this
 * 函数说明：设置视频消息描述
 */
this.wechat.description(description)

/**
 * 入参：description : List<Map<String, String>> 图文消息Article，具体属性参考me.chanjar.weixin.cp.bean.article.NewArticle
 * 返回值：this
 * 函数说明：设置视图文消息内容 
 */
this.wechat.article(articleList);

/**
 * 入参：multipartFile : MultipartFile 文件
 * 入参：parentId : Long 存放在[文件管理]的文件夹ID，通过在【文件管理】中“右键”文件夹，选择菜单[复制ID]获取
 * 入参：key: String 文件关键字
 * 返回值：this
 * 函数说明：上传媒体文件，同时会设置消息的mediaId和thumbMediaId属性
 */
this.wechat.uploadMedia(multipartFile, parentId, key);

/**
 * 入参：key: String 文件关键字,通过在【文件管理】中“右键”文件，选择菜单[复制KEY]获取
 * 返回值：this
 * 函数说明：上传文件管理中已经存在的媒体文件，同时会设置消息的mediaId和thumbMediaId属性
 */
this.wechat.uploadMedia(key);

/**
 * 返回值：WxCpMessageSendResult，请参考：me.chanjar.weixin.cp.bean.message.WxCpMessageSendResult
 * 函数说明：执行消息发送
 */
this.wechat.sendMessage();

/**
 * 入参：redirectUrl : String 用户授权完成后的重定向链接，无需urlencode, 方法内会进行encode
 * 返回值：String 重定向地址
 * 函数说明：构造oauth2授权的url连接.
 */
this.wechat.buildAuthorizationUrl(redirectUri)

/**
 * 入参：redirectUrl : String 用户授权完成后的重定向链接，无需urlencode, 方法内会进行encode
 * 入参：state : String 重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
 * 返回值：String 重定向地址
 * 函数说明：构造oauth2授权的url连接.
 */
this.wechat.buildAuthorizationUrl(redirectUri, state)

/**
 * 入参：redirectUrl : String 用户授权完成后的重定向链接，无需urlencode, 方法内会进行encode
 * 入参：scope : String 应用授权作用域，
 *              snsapi_base （不弹出授权页面，直接跳转，只能获取用户openid），
 *              snsapi_userinfo （弹出授权页面，可通过openid拿到昵称、性别、所在地。并且， 即使在未关注的情况下，只要用户授权，也能获取其信息 ）
 * 入参：state : String 重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
 * 返回值：String 重定向地址
 * 函数说明：构造oauth2授权的url连接.
 */
this.wechat.buildAuthorizationUrl(redirectUri, scope, state)

/**
 * 入参：code :String 通过成员授权获取到的code，最大为512字节。每次成员授权带上的code将不一样，code只能使用一次，5分钟未被使用自动过期。
 * 返回值：WxCpOauth2UserInfo 请参考：me.chanjar.weixin.cp.bean.WxCpOauth2UserInfo
 * 函数说明：根据code获取成员信息
 */
this.wechat.getUserInfo(code);
```