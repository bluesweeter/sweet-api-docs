## Plugin插件 <!-- {docsify-ignore-all} -->
***

> `plugin模块`是默认引入的模块，所以无需引入

PluginModule用于管理安装的插件，包括获取所有插件、启用插件、停止插件和启动插件等功能，同时提供调用插件方法接口；

## API说明
***

```js
/**
 * 调用插件方法
 * @param {string} pluginId 插件ID
 * @param {string} beanName 插件内bean名称
 * @param {string} methodName 插件内bean的方法名
 * @param {Object[]} args 插件内bean的方法的参数
 * @returns {Object}
 */
this.plugin.invoke(pluginId, beanName, methodName, ...args);

/**
 * 获取所有插件列表
 * @returns {JSONArray} 插件列表信息
 */
this.plugin.list();

/**
 * 获取所有插件列表
 * @param {string} pluginId 插件ID
 * @returns {JSONObject} 插件信息
 */
this.plugin.get(pluginId);

/**
 * 删除插件
 * @param {string[]} pluginIds 插件ID集合
 * @returns {Boolean} 是否成功
 */
this.plugin.delete(pluginIds);

/**
 * 加载插件
 * @param {string[]} pluginIds 插件ID集合
 * @returns {Boolean} 是否成功
 */
this.plugin.load(pluginIds);

/**
 * 卸载插件
 * @param {string[]} pluginIds 插件ID集合
 * @returns {Boolean} 是否成功
 */
this.plugin.unload(pluginIds);

/**
 * 启动插件
 * @param {string[]} pluginIds 插件ID集合
 * @returns {Boolean} 是否成功
 */
this.plugin.start(pluginIds);

/**
 * 停止插件
 * @param {string[]} pluginIds 插件ID集合
 * @returns {Boolean} 是否成功
 */
this.plugin.stop(pluginIds);

/**
 * 上传插件
 * @param {MultipartFile} file 插件文件
 * @returns {string} 插件安装路径
 */
this.plugin.uploadFile(file);
```