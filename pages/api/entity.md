## Entity实体帮助对象 <!-- {docsify-ignore-all} -->
***

> `entity模块`是默认引入的模块，所以无需引入

EntityModule用于生成id、unid、creater和updater相关字段信息，主要用于保存实体快速生成使用。

## API说明
***

```js
/**
 * 生成随机id值，利用Snowflake生成
 * @returns {JSONObject} cn.hutool.json.JSONObject
 */
entity.builder().id().build();

/**
 * 生成随机unid值
 * @returns {JSONObject} cn.hutool.json.JSONObject
 */
entity.builder().unid().build();

/**
 * 根据当前用户生产creater创建人相关信息，包括：
 * creater_id
 * creater
 * creater_ou_id
 * creater_ou_full_name
 * creater_ou_full_code
 * create_time
 * @returns {JSONObject} cn.hutool.json.JSONObject
 */
entity.builder().creater().build();

/**
 * 根据当前用户生产creater创建人相关信息，包括：
 * updater_id
 * updater
 * updater_ou_id
 * updater_ou_full_name
 * updater_ou_full_code
 * update_time
 * @returns {JSONObject} cn.hutool.json.JSONObject
 */
entity.builder().updater().build();

/**
 * 根据当前用户生产指定prefix前缀的信息
 * @param {string} prefix 前缀
 * @param {int} length 生成_time时要截取前缀的长度
 * @returns {JSONObject} cn.hutool.json.JSONObject
 */
entity.builder().prefix("pubisher", 1).build();
```