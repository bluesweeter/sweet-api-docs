## 日志 <!-- {docsify-ignore-all} -->
***
> `log模块`是默认引入的模块，所以无需引入

日志变量是用于代码中日志记录输出，使用方法与slf4j使用方法一致。

## API说明
***

```js
/**
 * 输出日志
 * @param {string} info 日志内容
 */
this.log.info(info);
```

![日志界面](../../_media/api/log.png ":size=70%")