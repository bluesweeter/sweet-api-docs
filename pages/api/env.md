## Spring环境变量 <!-- {docsify-ignore-all} -->
***

> `env模块`是默认引入的模块，所以无需引入

EnvModule用于获取Spring配置的配置参数。

## API说明
***

```js
/**
 * 获取配置参数值
 * @param {string} key 键
 * @returns {string} 键值
 */
this.env.get(key);

/**
 * 获取配置
 * @param key 配置项
 * @param defaultValue 未配置时的默认值
 * @returns {string} 键值
 */
this.env.get(key, defaultValue);
```