## Excel插件 <!-- {docsify-ignore-all} -->
***

> `Excel模块`是插件模块，需要在【插件管理】中安装插件才能使用。

`Excel模块`集成EasyExcel工具，实现Excel流式导入、流式导出等功能。

### API说明
***

```js
/**
 * 入参：name: String sheet名称
 * 返回值：ExcelSheet
 * 函数说明：设置sheet
 */
excel.sheet(name);

/**
 * 入参：field: String 数据库字段名
 * 入参：value: Object 中文名
 * 返回值：ExcelSheet
 * 函数说明：设置sheet头
 */
excel.sheet(name).header(field, value);

/**
 * 入参：heads: Map<String, String> 数据库字段名
 * 返回值：ExcelSheet
 * 函数说明：批量设置sheet头
 */
excel.sheet(name).header(heads);

/**
 * 入参：rowNumber: Integer 行号
 * 入参：row: Map<String, Object> 行数据
 * 返回值：ExcelSheet
 * 函数说明：设置行数据
 */
excel.sheet(name).row(rowNumber, row);

/**
 * 入参：rows: List<Map<String, Object>> 行数据列表
 * 返回值：ExcelSheet
 * 函数说明：批量增加行数据
 */
excel.sheet(name).row(rows);

/**
 * 入参：tplFile: MultipartFile 模板/数据文件
 * 返回值：ExcelSheet
 * 函数说明：设置模板文件/上传导入数据文件
 */
excel.sheet(name).file(MultipartFile tplFile);

/**
 * 入参：key: String 文件关键字，指定文件管理中的文件
 * 返回值：ExcelSheet
 * 函数说明：设置模板文件/上传导入数据文件
 */
excel.sheet(name).file(key);

/**
 * 入参：headRowNumber: Integer Excel头占多少行
 * 返回值：ExcelSheet
 * 函数说明：导入文件时，指定Excel头占多少行
 */
excel.sheet(name).headRowNumber(headRowNumber);

/**
 * 入参：fun: Function<List<Map<String, Object>>, Integer> 回调函数，输入：需要导入数据，输出：导入进去行数
 * 返回值：void
 * 函数说明：流式导入文件
 */
excel.sheet(name).execute(fun);

/**
 * 入参：fileName: String 文件名
 * 返回值：ResponseEntity<?>
 * 函数说明：普通导入文件
 */
excel.sheet(name).execute(fileName);

/**
 * 入参：fileName: String 文件名
 * 入参：pageFun: Function<Consumer<IPageResult>, Integer> 回调函数，输入：分页Consumer，导入行数
 * 返回值：ResponseEntity<?>
 * 函数说明：流式分页导入文件
 */
excel.sheet(name).execute(fileName, Function<Consumer<IPageResult>, Integer> pageFun);

```