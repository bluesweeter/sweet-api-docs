## Http请求 <!-- {docsify-ignore-all} -->
***

> `http模块`是默认引入的模块，所以无需引入

```text
`HttpModule`用于调用http请求获取响应数据。
1、支持GET、POST、DELETE、HEAD、OPTIONS、TRACE和PATCH请求。
2、支持params、header和body参数。
3、自动复制平台请求头必须参数；
4、支持请求头信息自定义；
```

## API说明
***

```js

/**
 * 入参：url : String 请求URL
 * 入参：values : Map<String, Object> 参数值，headers代表请求头，params代表参数，body代表请求体
 * 入参：functions: Function<Object, Object> 回调函数集合，第一个函数代表调用成功回调函数，第二个函数代表调用失败回调函数
 * 返回值：void
 * 函数说明：封装通用的JSON Post请求，利用回调函数接收返回响应内容
 */
this.http.post(url, values, Function<Object, Object>... functions);

/**
 * 入参：url : String 请求URL
 * 入参：values : Map<String, Object> 参数值，headers代表请求头，params代表参数
 * 入参：functions: Function<Object, Object> 回调函数集合，第一个函数代表调用成功回调函数，第二个函数代表调用失败回调函数
 * 返回值：void
 * 函数说明：封装通用的JSON Get请求，利用回调函数接收返回响应内容
 */
this.http.get(url, values, Function<Object, Object>... functions);

/**
 * 入参：url : String 地址URL
 * 返回值：this
 * 函数说明：创建连接
 */
this.http.connect(url);

/**
 * 入参：serviceName : String 服务名
 * 入参：path : String 路径
 * 返回值：this
 * 函数说明：本地服务调用
 */
this.http.connectService(serviceName, path)

/**
 * 入参：method : String 请求方法 GET|HEAD|POST|PUT|PATCH|DELETE|OPTIONS|TRACE
 * 返回值：this
 * 函数说明：设置请求方法，默认GET
 */
this.http.method(method);

/**
 * 入参：contentType : String Content-Type值
 * 返回值：this
 * 函数说明：设置`ContentType`
 */
this.http.contentType(contentType);

/**
 * 入参：body : String RequestBody
 * 返回值：this
 * 函数说明：设置`RequestBody`
 */
this.http.body(body);

/**
 * 入参：key : String header名
 * 入参：value : String header值
 * 返回值：this
 * 函数说明：设置header
 */
this.http.header(key, value);

/**
 * 入参：values : Map<String, Object> header值集合
 * 返回值：this
 * 函数说明：批量设置header
 */
this.http.header(Map<String, Object> values);

/**
 * 入参：headers : ...String headers值集合
 * 返回值：this
 * 函数说明：从当前请求批量复制头信息
 */
this.http.requestHeader(String... headers);

/**
 * 入参：key : String 参数名
 * 入参：values : ...Object 参数值
 * 返回值：this
 * 函数说明：设置URL参数
 */
this.http.param(key, ...values);

/**
 * 入参：values : Map<String, Object> 参数值
 * 返回值：this
 * 函数说明：批量设置URL参数
 */
this.http.param(values);

/**
 * 入参：key : String 参数名
 * 入参：values : ...Object 参数值
 * 返回值：this
 * 函数说明：设置form参数
 */
this.http.data(key, ...values);

/**
 * 入参：values : Map<String, Object> 参数值
 * 返回值：this
 * 函数说明：批量设置form参数
 */
this.http.data(values);

/**
 * 返回值：this
 * 函数说明：设置返回值为`byte[]`
 */
this.http.expectBytes();

/**
 * 返回值：this
 * 函数说明：设置返回值为`JSONObject`
 */
this.http.expectJson();

/**
 * 返回值：ResponseEntity<?>
 * 函数说明："发送`GET`请求"，用于设置参数后调用
 */
this.http.get();

/**
 * 返回值：ResponseEntity<?>
 * 函数说明："发送`POST`请求"，用于设置参数后调用
 */
this.http.post();

/**
 * 返回值：ResponseEntity<?>
 * 函数说明："发送`PUT`请求"，用于设置参数后调用
 */
this.http.put();

/**
 * 返回值：ResponseEntity<?>
 * 函数说明："发送`DELETE`请求"，用于设置参数后调用
 */
this.http.delete();

/**
 * 返回值：ResponseEntity<?>
 * 函数说明："发送`HEAD`请求"，用于设置参数后调用
 */
this.http.head();

/**
 * 返回值：ResponseEntity<?>
 * 函数说明："发送`OPTIONS`请求"，用于设置参数后调用
 */
this.http.options();

/**
 * 返回值：ResponseEntity<?>
 * 函数说明："发送`TRACE`请求"，用于设置参数后调用
 */
this.http.trace();
```