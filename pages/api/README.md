## db引入
***
> `db模块`是默认引入的模块，所以无需引入

`db模块`为`Sweet-API`提供数据库操作的API，通过`this.db`或`db`都能引入该对象，提供了以下三种模式：

* SQL操作：提供原生SQL数据库操作API，操作入口`db.`；

* Table操作：提供对单个TABLE数据库操作API，操作入口`db.table('table_name')`；

* SQL集合操作：提供对原生SQL组成的SQL集合数据操作API，操作入口`db.sql('SQL_UNION')`  

> `SQL集合操作`模式是在`Table操作`基础上扩展而来，基本的操作API与`Table操作`大致相同。

## 数据源配置
***

`db`数据源配置有两种方式：

* YAML文件配置

```yml
spring:
  datasource:
    dynamic:
      primary: master
      datasource:
        ## 默认数据源配置
        master:
          url: jdbc:mysql://${MYSQL_HOST:10.1.10.188}:${MYSQL_PORT:3306}/egrand-cloud-ky3h-ram?useUnicode=true&characterEncoding=utf8&characterSetResults=utf8&serverTimezone=GMT%2B8&connectTimeout=60000&socketTimeout=30000
          username: ${MYSQL_USERNAME:root}
          password: ${MYSQL_PASSWORD:123456}
          driverClassName: com.mysql.jdbc.Driver
          type: com.alibaba.druid.pool.DruidDataSource
        ## 另外一个数据源配置
        gpcsql:
          url: jdbc:sqlserver://10.1.10.144:1433;DatabaseName=egrand-cloud-gpc
          username: sa
          password: wiki2012..
          driverClassName: com.microsoft.sqlserver.jdbc.SQLServerDriver
          type: com.alibaba.druid.pool.DruidDataSource
```

* UI界面配置

![db数据源配置](../../_media/api/db-datasource.png ':size=60%')

## SQL操作

* ### select
***

```js
/**
 * 查询List结果
 * @param {string} sql `SQL`语句
 * @returns {List<Map<String, Object>} 查询List结果
 */
db.select("select * from news");

/**
 * 按条件查询List结果
 * @param {string} sql `SQL`语句
 * @param {Map<String, Object>} params 查询参数
 * @returns {List<Map<String, Object>} 查询List结果
 */
db.select("select * from news where id = ${id}", {id: 1});

/**
 * 查询int结果,需要保证结果返回一行一列
 * @param {string} sql `SQL`语句
 * @returns {Integer} 查询int结果
 */
db.selectInt('select count(*) from news');

/**
 * 按条件查询int结果,需要保证结果返回一行一列
 * @param {string} sql `SQL`语句
 * @param {Map<String, Object>} params 查询参数
 * @returns {Integer} 查询int结果
 */
db.selectInt('select count(*) from news where id = ${id}', {id: 1});

/**
 * 查询单个对象
 * @param {string} sql `SQL`语句
 * @returns {Map<String,Object>} 查询结果
 */
db.selectOne('select * from news limit 1');

/**
 * 按条件查询单个对象
 * @param {string} sql `SQL`语句
 * @param {Map<String, Object>} params 查询参数
 * @returns {Map<String,Object>} 查询结果
 */
db.selectOne('select * from news where id = ${id}', {id: 1});

/** 
 * 入参：sql:String sql select语句
 * 返回值：Object
 * 函数说明：查询单个值，需要保证结果返回一行一列
**/
/**
 * 查询单个值，需要保证结果返回一行一列
 * @param {string} sql `SQL`语句
 * @returns {Object} 单个值结果
 */
db.selectValue('select subject from news limit 1');

/** 
 * 入参：sql:String sql select语句
 * 返回值：List<Map<String,Object>> 查询参数
 * 返回值：Object
 * 函数说明：
**/
/**
 * 查询单个值，需要保证结果返回一行一列
 * @param {string} sql `SQL`语句
 * @param {Map<String, Object>} params 查询参数
 * @returns {Object} 单个值结果
 */
db.selectValue('select subject from news where id = ${id}', {id: 1});
```

* ### insert
***

```js
/**
 * 插入数据
 * @param {string} insertSql 插入SQL语句
 * @param {Map<String, Object>} params 参数
 * @returns 插入数据主键值
 */
db.insert("insert into news (subject, create_time, content) values ('${subject}', '${createTime}', '${content}')", {
    subject: '新闻标题',
    createTime: '2023-05-07 11:01:12',
    content: '新闻内容'
});

/**
 * 插入数据(指定主键列)
 * @param {string} insertSql 插入SQL语句
 * @param {string} primary 主键列
 * @param {Map<String, Object>} params 参数
 * @returns 插入数据主键值
 */
db.insert("insert into news (subject, create_time, content) values ('${subject}', '${createTime}', '${content}')", "id", {
    subject: '新闻标题',
    createTime: '2023-05-07 11:01:12',
    content: '新闻内容'
});
```

* ### update
***

```js

/**
 * 更新数据
 * @param {string} sql `Update SQL`语句
 * @returns {int} 受影响行数
 */
db.update("update news set subject= '修改新闻标题' where id = 1");

/**
 * 更新数据
 * @param {string} sql `Update SQL`语句
 * @param {Map<String, Object>} params 参数
 * @returns {int} 受影响行数
 */
db.update("update news set subject = '${subject}' where id = ${id}", {id: 1, subject: "修改新闻标题"});

/**
 * 批量更新数据
 * @param {string} sqls `Update SQL`语句数组
 * @returns {int} 受影响行数
 */
db.batchUpdate(["update news set subject= '1' where id = 113", "update news set subject= '2' where id = 114"]);

/**
 * 批量更新数据
 * @param {string} sql `Update SQL`语句
 * @param {List<Object[]>} args 参数
 * @returns {int} 受影响行数
 */
db.batchUpdate("update news set subject = ? where id = ?", [["10", 115]]);

/**
 * 批量更新数据
 * @param {string} sql `Update SQL`语句
 * @param {int} batchSize 批量插入每次数量
 * @param {List<Object[]>} args 参数
 * @returns {int} 受影响行数
 */
db.batchUpdate("update news set subject = ? where id = ?", 2, [["10", 115]]);
```

* ### page
***

```js
/**
 * @typedef {Object} PageInfo 分页信息
 * @property {number} page 当前页
 * @property {number} pages 总页数
 * @property {number} limit 总页数限制条数
 * @property {number} total 总条数
 * @property {Object} records 数据列表
 * @property {string} ascs 请求时正序字段(多个用逗号隔开)
 * @property {string} descs 请求时倒序字段(多个用逗号隔开)
 */

/**
 * 获取分页
 * @param {string} pageSql 分页SQL语句
 * @param {long} limit 限制条数
 * @param {long} offset 跳过条数
 * @returns {PageInfo} 分页信息
 */
db.page('select * from news', 25, 0);

/**
 * 获取分页
 * @param {string} pageSql 分页SQL语句
 * @param {long} limit 限制条数
 * @param {long} offset 跳过条数
 * @param {Map<String, Object>} params 参数
 * @returns {PageInfo} 分页信息
 */
db.page('select * from news where subject like ${key}', 25, 0, {key: '%测试%'});

/**
 * 获取分页
 * @param {string} pageSql 分页SQL语句
 * @param {long} page 页码
 * @param {long} limit 每页条数
 * @returns {PageInfo} 分页信息
 */
db.page1('select * from news', 1, 25);

/**
 * 获取分页
 * @param {string} pageSql 分页SQL语句
 * @param {long} page 页码
 * @param {long} limit 每页条数
 * @param {Map<String, Object>} params 参数
 * @returns {PageInfo} 分页信息
 */
db.page1('select * from news where subject like ${key}', 1, 25, {key: '%测试%'});
```

* ### delete
***

```js
/**
 * 删除数据
 * @param {string} sql `Delete SQL`语句
 * @returns {int} 受影响行数
 */
db.update('delete from news where id = 1');

/**
 * 删除数据
 * @param {string} sql `Delete SQL`语句
 * @param {Map<String, Object>} params 参数
 * @returns {int} 受影响行数
 */
db.update('delete from news where id = {id}', {id: 1});
```

## Table操作

操作入口：`db.table('table_name')`

* ### select
***

```js
/**
 * 查询List结果(与db.select作用相同)
 * @returns {List<Map<String, Object>} 查询List结果
 */
db.table("news").select();

/**
 * 查询指定列List结果
 * @returns {List<Map<String, Object>} 查询List结果
 */
db.table("news").columns("id, subject").select();
db.table("news").columns("id", "subject").select();
db.table("news").columns(["id", "subject"]).select();

/**
 * 排除指定列List结果
 * @returns {List<Map<String, Object>} 查询List结果
 */
db.table("news").excludes("id", "subject").select();
db.table("news").excludes(["id", "subject"]).select();

/**
 * 查询结果指定字段排序
 * @returns {List<Map<String, Object>} 查询List结果
 */
db.table("news").orderBy(["id", "subject"]).select();
db.table("news").orderBy("id").orderBy("subject").select();
db.table("news").orderBy("id", "desc").orderBy("subject", "asc").select();

/**
 * 查询结果指定字段排序
 * @returns {List<Map<String, Object>} 查询List结果
 */
db.table("news").orderByDesc(["id", "subject"]).select();
db.table("news").orderByDesc("id").orderByDesc("subject").select();

/**
 * 设置where查询条件
 * eq --> ==
 * ne --> <>
 * lt --> <
 * gt --> >
 * lte --> <=
 * gte --> >=
 * in --> in
 * notIn --> not in
 * like --> like
 * notLike --> not like
 * 函数说明：where条件查询
 */
db.table("news").where().eq("id", 1).like("subject", '%测试%').select();
```

* ### insert
***

```js
/**
 * 插入数据
 * @param {Map<String, Object>} data 插入的数据
 * @returns {number} 插入数据主键值
 */
db.table("news").insert({subject: "单表插入数据", create_time: "2023-05-07 11:20:30", content: "单表插入数据"});

/**
 * 插入数据(指定主键列)
 * @param {Map<String, Object>} data 插入的数据
 * @returns {number} 插入数据主键值
 */
db.table("news").primary("id").insert({subject: "单表插入数据", create_time: "2023-05-07 11:20:30", content: "单表插入数据"});

/**
 * 批量插入数据
 * @param {List<Map<String, Object>>} data 插入的数据
 * @returns {number} 受影响行数
 */
db.table("news").batchInsert([
    {
        subject: "单表插入数据", 
        create_time: "2023-05-07 11:20:30", 
        content: "单表插入数据"
    },{
        subject: "单表插入数据", 
        create_time: "2023-05-07 11:20:30", 
        content: "单表插入数据"
    }
]);

/**
 * 批量插入数据
 * @param {List<Map<String, Object>>} data 插入的数据
 * @param {number} batchSize batchSize
 * @returns {number} 受影响行数
 */
db.table("news").batchInsert([
    {
        subject: "单表插入数据", 
        create_time: "2023-05-07 11:20:30", 
        content: "单表插入数据"
    },{
        subject: "单表插入数据", 
        create_time: "2023-05-07 11:20:30", 
        content: "单表插入数据"
    }
], 10);
```

* ### update
***

> 在更新单表数据时，为避免误操作更新了所有数据，所以要求在更新时配合`where`或指定更新数据的`主键值`才运行，否则报异常错误。

```js
/**
 * 更新数据,配合指定更新数据的`主键值`运行
 * @param {Map<String, Object>} data 更新数据
 * @returns {number} 执行SQL后影响行数
 */
db.table("news").primary("id").update({id: 129, subject: "单表更新"});

/**
 * 更新数据,配合`where`条件运行
 * @param {Map<String, Object>} data 更新数据
 * @returns {number} 执行SQL后影响行数
 */
db.table("news").where().eq("id", 129).update({subject: "单表更新"});

/**
 * 更新数据,配合`where`条件运行
 * @param {Map<String, Object>} data 更新数据
 * @param {boolean} isUpdateBlank 是否更新空值字段
 * @returns {number} 执行SQL后影响行数
 */
db.table("news").where().eq("id", 129).update({"subject": "测试"}, true);

```

* ### page
***

```js
/**
 * @typedef {Object} PageInfo 分页信息
 * @property {number} page 当前页
 * @property {number} pages 总页数
 * @property {number} limit 总页数限制条数
 * @property {number} total 总条数
 * @property {Object} records 数据列表
 * @property {string} ascs 正序排序字段
 * @property {string} descs 倒序排序字段
 */

/**
 * 获取分页
 * @param {long} limit 限制条数
 * @param {long} offset 跳过条数
 * @returns {PageInfo} 分页信息
 */
db.table("news").page(25, 0);

/**
 * 获取分页
 * @param {long} page 页码
 * @param {long} limit 每页条数
 * @returns {PageInfo} 分页信息
 */
db.table("news").page1(1, 10);

/**
 * 获取分页(where条件)
 * 设置where条件
 * eq --> ==
 * ne --> <>
 * lt --> <
 * gt --> >
 * lte --> <=
 * gte --> >=
 * in --> in
 * notIn --> not in
 * like --> like
 * notLike --> not like
 * @param {long} page 页码
 * @param {long} limit 每页条数
 * @returns {PageInfo} 分页信息
 */
db.table("news").where().eq("id", 265).like("subject", '%事务%').page1(1, 20);
```

* ### delete
***

> 在删除单表数据时，为避免误操作删除了所有数据，所以要求在删除时配合`where`才运行，否则报异常错误。

```js
/**
 * 删除数据,需要配合`where`才运行
 * @returns {number} 执行SQL后影响行数
 */
db.table("news").where().eq("id", 129).delete();
```
* ### 其他
***

```js
/**
 * 获取数量
 * @returns {int} 数量
 */
db.table("news").count();

/**
 * 判断是否存在
 * @returns {boolean} 是否存在数据
 */
db.table("news").where().eq("id", 1).exists();
```

## SQL集合操作

操作入口：`db.sql('SQL_UNION')`，提供SQL集合操作，操作接口与`db.table('table_name')`一致。

```js
/**
 * 查询List结果
 * @returns {List<Map<String,Object>>} List结果
 */
db.sql("select * from news union select * from news where subject like '%测试%'").select();

/**
 * 查询List结果(where条件)
 * @param {string} sql `Union SQL`语句
 * @param {List<Object[]>} args 参数
 * @returns {List<Map<String,Object>>} List结果
 */
db.camel().sql("select * from news union select * from news where subject like '%${subject}%'", {subject: "测试"})
    .where()
    .eq(model.hdlb_id !== undefined, "hdlb_id", model.hdlb_id)
    .in(model.status !== undefined, "status", model.status)
    .gte(model.start_date !== undefined, "start_date", model.start_date)
    .lte(model.end_date !== undefined, "end_date", model.end_date)
    .and(model.key !== undefined, it => {
        let key = "%" + model.key + "%";
        return it.like("subject", key)
            .or().like("description", key)
            .or().like("place", key)
            .or().like("record", key);
    })
    .orderBy(this.body.ascs.split(","))
    .orderByDesc(this.body.descs.split(","))
    .page1(page, limit);
```