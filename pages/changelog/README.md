# 更新日志  <!-- {docsify-ignore-all} -->

## [v1.0.0] 2024.03.02
***

* 增加[PoiModule](/pages/api/poi)，提供WordModule和WordUtil操作word文档；

* 增加[HttpModule](/pages/api/http)，用于调用http请求获取响应数据，替代[RESTFUL](/pages/api/restful)插件；

## [v1.0.0] 2023.06.25
***

* 增加[AssertModule](/pages/api/assert)断言定义模块，在编写代码可以使用相关接口实现断言判断；

## [v1.0.0] 2023.06.16
***

* Sweet-API重大变更：将`Sweet-API`从`微服务平台`中独立，不强制依赖微服务平台的`用户和认证体系`；

* Sweet编辑器支持`不登录模式`；

* Sweet编辑器增加[系统配置](/pages/?id=系统配置)功能，用于切换不同的Sweet-API后端服务，同时删除`登录界面`的配置功能；

## [v1.0.0] 2023.05.29
***

* 去掉[Response插件](/pages/api/response)的Feign`download`接口；

* 去掉[Request插件](/pages/api/request)的`getEgdHeader`接口；

* 优化[RabbitMQ插件](/pages/plugin/mq)，增加发送时设置请求头接口，去掉`sendUser`监听参数，用`header`监听参数替代等;

* 修复API脚本`表达式验证`功能；

* 修复DB组件exclude排除一系列方法无效问题，修改为：根据用户实际输出字段模式来排除；
  
  例如：字段user_name,默认情况下用exclude("user_name")来排除，如果使用camel()，则排除为exclude("userName")。

* 修复API分组拖拽移动功能；

## [v1.0.0] 2023.05.27
***

* 增加[API执行器](/pages/quick/apiActuator)功能：通过为`API`指定 `执行器`，扩展API调用方式。

* 增加[RabbitMQ插件](/pages/plugin/mq)：集成`RabbitMQ`工具，实现异步消息队列通信API。

* 增加[Task插件](/pages/plugin/task)：集成SpringBoot内置的定时任务调度，满足基本的调度要求。

* 增加[RabbitMQ消费者执行器](/pages/plugin/mq?id=使用)：指定`API`作为`MQ的消费端`，通过指定的监听`MQ数据源`和`数据队列名称`来执行API。

* [SweetFile插件](/pages/api/file)增加`上传文件到指定目录`API；

* [Sweet插件](/pages/api/sweet)增加`不带context的API执行接口`API；

* 修复`db插件`的`SQL操作`在`MYSQL数据库`别名错误问题；

* 修复导出功能：当API分组parentId为null时，导出时无法显示该分组问题；

## [v1.0.0] 2023.05.24
***

* 增加[ExcelModule](/pages/plugin/excel)插件，集成EasyExcel实现Excel流式导入、流式导出等功能；

* [RESTfulModule](/pages/api/restful) 添加`expectJson()`方法，用于指定请求接口`响应类型(responseType)`为JSONObject，默认为String；

* [RESTfulModule](/pages/api/restful) 添加`connectService(serviceName, path)`直接调用本地微服务接口，实例参考：[本地微服务调用](/pages/platform/restful)；

* [RESTfulModule](/pages/api/restful) 添加`header(Map<String, Object> values)`方法批量设置请求头；

* [RequestModule](/pages/api/request) 添加`getEgdHeader`方法，用于获取自定义请求头集合；

*  `接口`和`数据源`界面增加缓存刷新按钮，用于刷新后端的API和DataSource缓存；

* `编辑器`添加简单语法提示:`bre`、`con`、`if`、`ife`、`fori`、`forof`、`exit`、`logi`、`logd`、`loge`、`logw`；

* 增加[ES6常用语法](/pages/script/syntax)指导文档；

* 修复`登录`时`网关地址`修改无效问题；

* 修复`导出`功能，当API路径为空时无法显示问题；