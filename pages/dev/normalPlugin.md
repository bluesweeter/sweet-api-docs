## 插件示例 <!-- {docsify-ignore-all} -->

> `普通插件`开发相对`ESB插件`开发较为简单，只需按照`插件简介`搭建完成，实现本身插件逻辑即可，请参考GitHub上：[普通插件开发示例](http://10.1.10.211/egrand-cloud-demo/egrand-cloud-demo-plugin/tree/master/egrand-cloud-demo-plugin-normal)