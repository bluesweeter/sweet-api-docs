## 插件Maven结构

```markdown
egrand-cloud-plugin-[项目简称]-[插件简称]
├─src
│ └─main
|   ├─assembly
|   | └─assembly.xml                         --- 插件打包描述文件
│   ├─java
│   │ └─com
│   │   └─egrand
│   │     └─cloud
│   │       └─plugin      
|   |         └─ [项目简称]  
|   |            └─[插件简称]                 --- 插件包名
|   |              └─xxxConfiguration.java   --- 插件配置类     
|   |              └─xxxPlugin.java          --- 插件入口类
│   └─resources
│     └─bootstrap.yml                        --- Spring配置文件
└ pom.xml                                    --- pom.xml 
```
> 例如：Redis插件名称为：egrand-cloud-plugin-esb-redis

## pom.xml配置

下面以`Redis`插件为例，分段介绍`pom.xml`中相关配置

> parent配置，父pom.xml定义了基础依赖

```xml
<parent>
    <artifactId>egrand-cloud-plugins</artifactId>
    <groupId>com.egrand.cloud</groupId>
    <version>1.2.1-SNAPSHOT</version>
</parent>
```

> properties配置，定义了插件相关信息属性

```xml
<properties>
    <main.basedir>${basedir}/../..</main.basedir>
    <!-- 插件ID -->
    <plugin.id>egrand-cloud-plugin-esb-wechat</plugin.id>
    <!-- 插件入口类 -->
    <plugin.class>com.egrand.cloud.plugin.esb.wechat.EsbWechatPlugin</plugin.class>
    <!--插件版本-->
    <plugin.version>${revision}</plugin.version>
    <!-- 插件提供商 -->
    <plugin.provider>Egrand</plugin.provider>
    <!-- 插件描述 -->
    <plugin.description>ESB中Wechat插件定义，为Sweet-API提供Wechat操作接口</plugin.description>
    <!-- 插件依赖关系-->
    <plugin.dependencies />
</properties>
```

> dependencies依赖

插件中所有依赖均采用`<scope>provided</scope>`，编译打包时默认不会打包到`lib`目录下，需要打包到`lib`，请在`assembly.xml`中指定。

```xml
<dependencies>
    <!-- 插件本身依赖，这里根据自己插件需要引用依赖 -->
    ...
</dependencies>
```

> build配置，不需要修改，直接复制即可

```xml
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-antrun-plugin</artifactId>
            <version>1.8</version>
            <executions>
                <execution>
                    <id>unzip jar file</id>
                    <phase>package</phase>
                    <configuration>
                        <target>
                            <unzip src="target/${project.artifactId}-${project.version}.${project.packaging}" dest="target/plugin-classes" />
                        </target>
                    </configuration>
                    <goals>
                        <goal>run</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-assembly-plugin</artifactId>
            <version>2.3</version>
            <configuration>
                <descriptors>
                    <descriptor>
                        src/main/assembly/assembly.xml
                    </descriptor>
                </descriptors>
                <appendAssemblyId>false</appendAssemblyId>
            </configuration>
            <executions>
                <execution>
                    <id>make-assembly</id>
                    <phase>package</phase>
                    <goals>
                        <goal>attached</goal>
                    </goals>
                </execution>
            </executions>
        </plugin>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-jar-plugin</artifactId>
            <version>3.1.2</version>
            <configuration>
                <archive>
                    <manifestEntries>
                        <Plugin-Id>${plugin.id}</Plugin-Id>
                        <Plugin-Class>${plugin.class}</Plugin-Class>
                        <Plugin-Version>${plugin.version}</Plugin-Version>
                        <Plugin-Provider>${plugin.provider}</Plugin-Provider>
                        <Plugin-Description>${plugin.description}</Plugin-Description>
                        <Plugin-Dependencies>${plugin.dependencies}</Plugin-Dependencies>
                    </manifestEntries>
                </archive>
            </configuration>
        </plugin>
        <plugin>
            <artifactId>maven-deploy-plugin</artifactId>
            <version>2.8.2</version>
            <configuration>
                <skip>false</skip>
            </configuration>
        </plugin>
    </plugins>
</build>
```

## assembly.xml配置

> assembly.xml用于自定义打包插件描述。

如果需要将指定包打包到`lib`下，可以在`assembly.xml`开启`<scope>provided</scope>`，同时通过`include`方式手工指定，不包括它的相关依赖，如果它的相关依赖也需要打包到`lib`，需要逐一指定。

所以，如果依赖比较多，建议将依赖声明到`ESB主项目`中。

```xml
<assembly>
	<id>plugin</id>
	<formats>
		<!--插件打包格式 -->
		<format>zip</format>
	</formats>
	<includeBaseDirectory>false</includeBaseDirectory>
	<dependencySets>
		<dependencySet>
			<useProjectArtifact>false</useProjectArtifact>
			<!-- 第三方JAR存放目录 -->
			<outputDirectory>lib</outputDirectory>
			<!--插件打包名称-->
			<outputFileNameMapping>${artifact.artifactId}-${artifact.baseVersion}.${artifact.extension}</outputFileNameMapping>
			<!-- 将scope为provided的依赖包打包到lib目录下。 -->
			<!--<scope>provided</scope>-->
			<!-- 手工逐一指定需要打包到lib文件夹的Jar，组成方式：[groupId]:[artifactId] -->
			<!--<includes>-->
			<!--	<include>com.egrand.cloud:egrand-cloud-starter-redis</include>-->
			<!--</includes>-->
		</dependencySet>
	</dependencySets>
    <fileSets>
        <fileSet>
            <directory>target/plugin-classes</directory>
            <outputDirectory>classes</outputDirectory>
        </fileSet>
    </fileSets>
</assembly>
```

## 插件入口类

> 插件入口类是启动插件初始化`WebApplication`时调用，没有特殊情况下代码都一样，只是类名称不同而已；

```java
/**
 * 继承SpringPlugin，添加一个构造方法即可
 */
public class EsbRedisPlugin extends SpringPlugin {

    public EsbRedisPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }

}
```

## 插件配置类

> 插件配置类采用`@Configuration`注解，使用方式与`SpringBoot`配置类使用方式一样。在插件启动时会自动完成配置，一般用于定义插件`Bean`，初始化插件自身配置文件参数等。

```java
@ComponentScans(value = {
        @ComponentScan(value = "com.egrand.cloud.redis")
})
@Configuration
public class EsbRedisConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public RedisModule redisModule(RedisTemplate redisTemplate) {
        return new RedisModule(redisTemplate);
    }
}
```

## 插件编译

> 插件编写完成后，运行`mvn clean install`打包，运行`mvn clean deploy`发布。插件打包后是`zip`格式的，安装插件时会`自动解压zip文件`。

```markdown
egrand-cloud-plugin-[项目简称]-[插件简称].zip
├─classes
│ └─com
│ |   └─egrand
│ |    └─cloud
│ |      └─plugin      
| |        └─ [项目简称]  
| |           └─[插件简称]                 --- 插件编译class类
| └─META-INF                              --- 插件描述信息
|─lib                                     --- 插件依赖的jar包都放在这个文件夹里面

```