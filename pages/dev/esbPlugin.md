## ESB插件

```text
ESB插件与一般的插件不同，在`Sweet-API`前端界面的`插件`页签中安装后，可以通过定义的关键字进行直接引用。

例如：Redis插件安装后，通过`this.redis`入口调用定义的Java方法。

```
## pom.xml依赖

> 开发`ESB插件`需要添加`egrand-cloud-plugin-esb`包依赖。里面提供了开发`ESB插件`需要用到的`接口`和`顶层类`。

```xml
...
<dependencies>
    <!-- 开发ESB Module插件，需要依赖 -->
    <dependency>
        <groupId>com.egrand.cloud</groupId>
        <artifactId>egrand-cloud-plugin-esb</artifactId>
        <scope>provided</scope>
    </dependency>
</dependencies>
...
```

## egrand-cloud-plugin-esb介绍

> `egrand-cloud-plugin-esb`定义了开发`ESB插件`需要的`接口`和`顶层类`，采用`工厂模式`动态发现插件模块。

```markdown
egrand-cloud-plugin-esb
├─src
│ └─main
│   ├─java
│     └─com
│       └─egrand
│         └─cloud
│           └─plugin      
|             └─ esb  
|                └─ConnectionAdapteService.java      --- 连接Adapte接口,对接前端数据源界面
|                └─LocalFileService.java             --- ESB提供的文件服务接口，实现该接口可以扩展文件服务管理方式
|                └─LocalFileServiceFactory.java      --- ESB提供的文件服务工厂，获取文件服务Service
|                └─ModuleService.java                --- ESB模块接口
└ pom.xml                                            --- pom.xml
```

* **`ConnectionAdapteService.java`**

> 如果开发的ESB插件需要使用`Sweet-API`界面上`数据源`配置，则需要实现该接口。

```java
public interface ConnectionAdapteService extends PluginService {

    /**
     * 启动应用程序时运行，用于初始化连接到连接池
     * @return
     */
    Boolean initialize(List<ConnectionBaseDTO> connectionBaseDTOList);

    /**
     * 删除连接时运行，用于执行删除后的清理动作
     * @return
     */
    Boolean destroy();

    /**
     * 新增连接时运行
     * @param connectionBaseDTO
     * @return
     */
    Boolean save(ConnectionBaseDTO connectionBaseDTO);

    /**
     * 更新连接时运行
     * @param connectionBaseDTO
     * @param oldKey
     * @return
     */
    Boolean update(ConnectionBaseDTO connectionBaseDTO, String oldKey);

    /**
     * 删除连接时运行
     * @param key
     * @return
     */
    Boolean delete(String key);

    /**
     * 编码连接信息
     * @return
     */
    void encode(Connection connection, ConnectionBaseDTO connectionBaseDTO);

    /**
     * 解码连接信息
     * @param connection
     * @param connectionBaseDTO
     */
    ConnectionBaseDTO decode(Connection connection, ConnectionBaseDTO connectionBaseDTO);

    /**
     * 测试连接
     * @param connectionInfo
     * @return
     */
    ConnectionBaseDTO test(String connectionInfo);
}
```

* **`LocalFileService.java`**

ESB提供的`文件服务扩展接口`，实现接口可以扩展文件管理服务实现，目前平台提供了`Local模式`和`ATM本地模式`：

1）`Local模式`：本地文件管理，也是默认配置模式，具体实现请参考：`com.egrand.cloud.esb.server.service.impl.LocalFileServiceImpl`实现类；

2）`ATM模式`：对接平台附件管理微服务，具体实现请参考：`com.egrand.cloud.esb.server.service.impl.ATMFileServiceImpl`实现类；

```java
public interface LocalFileService {

    /**
     * 文件服务-本地文件类型
     */
    String FILE_SERVICE_TYPE_LOCAL = "LOCAL";

    /**
     * 文件服务-ATM类型
     */
    String FILE_SERVICE_TYPE_ATM = "ATM";

    /**
     * 获取临时目录文件
     * @param fileName 文件名
     * @return 路径
     * @throws IOException
     */
    default String getTmpFilePath(String fileName) throws IOException {
        return this.getPath("/tmp", fileName);
    }

    /**
     * 获取模板目录文件
     * @param fileName 文件名
     * @return 路径
     * @throws IOException
     */
    default String getTplFilePath(String fileName) throws IOException {
        return this.getPath("/tpl", fileName);
    }

    /**
     * 上传模板文件
     * @param file 文件
     * @return 上传后路径
     * @throws IOException
     */
    default String uploadTemplateFile(MultipartFile file) throws IOException {
        String tplFilePath = this.getTplFilePath(file.getOriginalFilename());
        java.io.File targetFile = new java.io.File(tplFilePath);
        file.transferTo(targetFile);
        return tplFilePath;
    }

    /**
     * 获取ClassPathResource指定文件夹文件路径
     * @param folderName 文件夹名称
     * @param fileName 文件名称
     * @return 路径
     * @throws IOException
     */
    default String getPath(String folderName, String fileName) throws IOException {
        ClassPathResource classPathResource = new ClassPathResource(folderName);
        String tmpPath = classPathResource.getFile().getPath();
        String extendName = "";
        String name = "";
        if (fileName.lastIndexOf(".") != -1) {
            extendName = fileName.substring(fileName.lastIndexOf(".") + 1);
            name = fileName.substring(0, fileName.lastIndexOf("."));
        } else {
            name = fileName;
        }
        return tmpPath + java.io.File.separator + name + System.currentTimeMillis() + "." + extendName;
    }

    /**
     * 上传文件
     * @param egdUser 当前用户
     * @param file 文件
     * @param parentId 父文件夹ID
     * @param key 关键字
     * @return
     */
    File upload(EgdUser egdUser, MultipartFile file, Long parentId, String key) throws IOException;

    /**
     * 根据关键字获取文件路径
     * @param egdUser 当前用户
     * @param key 关键字
     * @return
     */
    String getFilePath(EgdUser egdUser, String key) throws IOException, OpenException;

    /**
     * 根据关键字下载文件
     * @param egdUser 当前用户
     * @param key 关键字
     * @return
     * @throws IOException
     */
    ResponseEntity<?> download(EgdUser egdUser, String key) throws IOException;

    /**
     * 获取类型
     * @return
     */
    abstract String getType();
}
```

* **`LocalFileServiceFactory.java`**

> 工厂类，为其他`ESB插件`提供文件服务接口服务。例如：开发企业微信时，需要发送`媒体消息`，可以将`上传的媒体文件保存下来`，然后提交到外网企业微信，方便下次调用文件。

```java
public class WechatModule implements ModuleService {

    private LocalFileServiceFactory localFileServiceFactory;

    public WechatModule(WxCpTemplate wxCpTemplate, LocalFileServiceFactory localFileServiceFactory) {
        this.localFileServiceFactory = localFileServiceFactory;
        this.wxCpTemplate = wxCpTemplate;
    }
    ...
    /**
     * 上传媒体文件
     * @param multipartFile 文件
     * @param parentId 存放文件夹ID
     * @param key 文件关键字
     * @return
     * @throws IOException
     * @throws WxErrorException
     */
    public WechatModule uploadMedia(MultipartFile multipartFile, Long parentId, String key) throws IOException, WxErrorException {
        InputStream is = null;
        try {
            File file = this.localFileServiceFactory.getService(LocalFileService.FILE_SERVICE_TYPE_LOCAL).upload(null, multipartFile, parentId, key);
            ...
        } finally {
            if(null != is)
                is.close();
        }
        return this;
    }
    ...
}
```

* **`ModuleService.java`**

> ESB模块需要实现`ModuleService`接口。

## 示例

请参考GitHub上：[ESB插件开发示例](http://10.1.10.211/egrand-cloud-demo/egrand-cloud-demo-plugin/tree/master/egrand-cloud-demo-plugin-esb)