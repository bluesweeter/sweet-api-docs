## 介绍
***

> `excel插件`是插件模块，需要在【插件管理】中安装插件才能使用。

`excel插件`集成了EasyExcel为Sweet-API提供Excel操作API，更多API请参考：[Excel API ](/pages/api/excel)

## 使用

导出带Excel头的数据

```js
return excel
    .sheet("数据")
    .head("ouName", "组织名称")
    .head("ouCode", "组织编码")
    .row(db.camel().table("egd_org_ouinfo").select())
    .execute("test");
```

导入数据

```js
let dataFile = request.getFile("file");
if (null == dataFile)
    return request.exit("-9", "请上传数据文件");
return result = excel
    .sheet("数据")
    .headRowNumber(2)
    .file(dataFile)
    .head("unid", "唯一编码")
    .head("subject", "标题")
    .head("creater", "创建人")
    .head("createTime", "创建时间")
    .execute(rowList => db.camel().table("egd_bulletin").batchInsert(rowList));
```