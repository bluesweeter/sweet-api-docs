## 插件安装 <!-- {docsify-ignore-all} -->

![插件安装](../../_media/plugin/pluginInstall.png ':size=70%')

* 左侧页签切换至`插件`页签，点击顶部工具条上`上传插件`按钮；

* 在弹出的插件安装对话框中选择需要安装的插件；

* 点击`上传`按钮即可完成插件安装；

> `Sweet-API`自带一些常用插件。

* [Redis插件](http://10.1.10.211/egrand-cloud/egrand-cloud-project/tree/master/egrand-cloud-plugins/egrand-cloud-plugin-esb-redis)

* [Elasticsearch插件](http://10.1.10.211/egrand-cloud/egrand-cloud-project/tree/master/egrand-cloud-plugins/egrand-cloud-plugin-esb-es)

* [Wechat插件](http://10.1.10.211/egrand-cloud/egrand-cloud-project/tree/master/egrand-cloud-plugins/egrand-cloud-plugin-esb-wechat)

* [RESTful插件](http://10.1.10.211/egrand-cloud/egrand-cloud-project/tree/master/egrand-cloud-plugins/egrand-cloud-plugin-esb-restful)