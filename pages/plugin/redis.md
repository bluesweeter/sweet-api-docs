## 介绍
***

> `redis插件`是插件模块，需要在【插件管理】中安装插件才能使用。

`redis插件`提供redis数据源访问API，更多API请参考：[Redis API ](/pages/api/redis)

## 数据源配置
***

`redis`数据源配置有两种方式：

* YAML文件配置

```yml
spring:
  redis:
    ## 开启redis多数据源
    enable-multi: true
    multi:
      ## 默认数据源
      default:
        database: 1
        host: 10.1.10.178
        password:
        port: 6364
        timeout: 60000
        lettuce:
          pool:
            max-active: -1
            max-idle: -1
            max-wait: -1
            min-idle: -1
        enable-multi: true
      ## 配置的数据源
      two:
        database: 1
        host: 10.1.10.178
        port: 6370
        password:
        timeout: 60000
        lettuce:
          pool:
            max-active: -1
            max-idle: -1
            max-wait: -1
            min-idle: -1
```

* UI界面配置

![Redis数据源配置](../../_media/plugin/redis-datasource.png ':size=60%')

## 使用

> `默认数据源`操作

```js
let value = "sweet-api";
// 设置redis的键值
this.redis.set("key", value);
// 获取redis键值
return this.redis.get("key");
```

> `指定数据源`操作：通过提供的`rs`方法指定需要访问的数据源

```js
let value = "sweet-api";
// 通过`rs`方法指定redis数据源设置键值
this.redis.rs("spb").set("key", value);
// 通过`rs`方法获取指定redis数据源的键值
return this.redis.rs("spb").get("key");
```

