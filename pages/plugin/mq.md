## 介绍
***

> `RabbitMQ插件`是插件模块，需要在【插件管理】中安装插件才能使用。

`RabbitMQ插件`集成`RabbitMQ`工具，为Sweet-API提供异步消息通信API，详细接口请参考[RabbitMQ插件API](/pages/api/mq.md)。

## 使用

通过`mq`关键字发送MQ消息。

```js
// 发送消息
mq.send("test2", 30000);
// 发送消息并设置头信息，x-tenant-header为内置，代表租户标识
mq.send("test2", 30000, {"x-tenant-header": this.context.tenant});
// 指定数据源发送消息
mq.mq("mq").send("test2", 30000);
// 发送消息并设置头信息，x-tenant-header为内置，代表租户标识
mq.mq("mq").send("test1", 50000, {"x-tenant-header": this.context.tenant});
```

通过`mq`关键字获取MQ消息

```js
this.log.info("头信息{}", headers);
this.log.info("获取头信息元素：{}", headers.get("x-tenant-header"));
this.log.info("获取头信息内容：{}", message);
```

在`API执行器`中设置MQ消费端参数，在API编辑器中通过`headers`内置变量获取MQ头信息，通过`message`内置变量获取发送过来的消息内容。

![执行器](../../_media/quick/apiActuator.png ':size=60%')

### 参数说明

* **名称**：为执行器定义一个名称；

* **类型**：选择"RabbitMQ"即可；

* **编码**：为执行器定义一个唯一编码；

* **执行用户**：不用设置；

* **扩展配置**：定义RabbitMQ扩展参数：

    - mqKey 指定数据源KEY

    - exchangeName 交换机名称

    - queueName 队列名称

    - qos 限流，设置接收消息条数限制

    - arguments 参数配置

        - x-max-length:消息条数限制,该参数是非负整数值。限制加入queue中消息的条数。先进先出原则，超过10条后面的消息会顶替前面的消息。

        - x-max-length-bytes:消息容量限制,该参数是非负整数值。该参数和x-max-length目的一样限制队列的容量，但是这个是靠队列大小（bytes）来达到限制。

        - x-message-ttl:消息存活时间,该参数是非负整数值.创建queue时设置该参数可指定消息在该queue中待多久，可根据x-dead-letter-routing-key和x-dead-letter-exchange生成可延迟的死信队列。

        - x-max-priority:消息优先级,创建queue时arguments可以使用x-max-priority参数声明优先级队列 。该参数应该是一个整数，表示队列应该支持的最大优先级。建议使用1到10之间。

          目前使用更多的优先级将消耗更多的资源（Erlang进程）。设置该参数同时设置死信队列时或造成已过期的低优先级消息会在未过期的高优先级消息后面执行。
          
          该参数会造成额外的CPU消耗。
        
        - x-expires:存活时间,创建queue时参数arguments设置了x-expires参数，该queue会在x-expires到期后queue消息，亲身测试直接消失（哪怕里面有未消费的消息）。

        - x-dead-letter-exchange和x-dead-letter-routing-key:创建queue时参数arguments设置了x-dead-letter-routing-key和x-dead-letter-exchahttp://nge，
        
          会在x-message-ttl时间到期后把消息放到x-dead-letter-routing-key和x-dead-letter-exchange指定的队列中达到延迟队列的目的。