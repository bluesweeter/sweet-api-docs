## 介绍
***

> `Wechat插件`是插件模块，需要在【插件管理】中安装插件才能使用。

`Wechat插件`提供微信应用数据源访问API，目前只支持企业微信对接，后续将扩展开发，更多API请参考：[Wechat API ](/pages/api/wechat)

## 数据源配置
***

`Wechat`数据源配置有两种方式：

* YAML文件配置

```yml
spring:
  wechat:
    cp:
      ## 启用多数据源
      enable-multi: true
      multi:
        ## 默认企业微信数据源
        default:
          agentId: xxx
          corpId: xxxx
          secret: xxxxx
          token: 111
          aesKey: 111
        ## 另外一个企业微信数据源
        zcyj:
          agentId: xxxx
          corpId: xxxx
          secret: xxxx
          token: 111
          aesKey: 111
```

* UI界面配置

![Wechat数据源配置](../../_media/plugin/wechat-datasource.png ':size=60%')

## 使用

> `默认数据源`操作

```js
// 发送文本信息
this.wechat.text().userId("tiger").content("Hello!!!").sendMessage();
```

> `指定数据源`操作：通过提供的`cp`方法指定需要访问的数据源

```js
// 通过`cp`方法指定数据源发送文本消息
this.wechat.cp("zcyj").text().userId("tiger").content("Hello!!!").sendMessage();
```