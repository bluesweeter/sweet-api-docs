## 介绍
***

> `Task插件`是插件模块，需要在【插件管理】中安装插件才能使用。

`Task插件`集成SpringBoot内置的定时任务调度，满足基本的调度要求。

## 配置

`Task插件`的`YML`配置文件配置参数如下：

```yml
dynamic-api:
  task:
    # 是否启用定时任务，默认为true
    enable: true
    # 线程池前缀，默认为esb-task-
    threadNamePrefix: 'esb-task-'
    # 线程池相关配置
    pool:
      # 线程池大小
      size: 10
    # 关闭时相关配置
    shutdown:
      # 关闭时是否等待任务执行完毕，默认为false
      awaitTermination: true
      # 关闭时最多等待任务执行完毕的时间
      awaitTerminationPeriod: 10
```

## 使用

![执行器](../../_media/plugin/task-setting.png ':size=60%')

扩展参数说明：

* **cron**：定时任务表达式，例如：0/2 * * * * ?（每2秒执行一次）

* **context**：运行时参数，JSON对象格式。

