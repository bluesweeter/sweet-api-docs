## 介绍
***

> `Elasticsearch插件`是插件模块，需要在【插件管理】中安装插件才能使用。

`Elasticsearch插件`提供Elasticsearch数据源访问API，更多API请参考：[Elasticsearch API ](/pages/api/es)

## 数据源配置
***

`ES`数据源配置有两种方式：

* YAML文件配置

```yml
spring:
  elasticsearch:
    ## 开启多数据源
    enableMulti: true
    multi:
      ## 默认ES数据源配置
      default:
        name: default
        elasticUser: elastic
        elasticPassword: changeme
        elasticsearch:
          rest:
            hostNames: ${ES_HOST:10.1.10.178}:${ES_PORT:9200}
          dateFormat: yyyy.MM.dd
          timeZone: Asia/Shanghai
          showTemplate: true
          discoverHost: false
        dslfile:
          refreshInterval: -1
        http:
          timeoutConnection: 5000
          timeoutSocket: 5000
          connectionRequestTimeout: 5000e
          retryTime: 1
          maxLineLength: -1
          maxHeaderCount: 200
          maxTotal: 400
          defaultMaxPerRoute: 200
          soReuseAddress: false
          soKeepAlive: false
          timeToLive: 3600000
          keepAlive: 3600000
          keystore:
          keyPassword:
          hostnameVerifier:
      ## 另外一个ES数据源配置
      spb:
        name: dpra-es
        elasticUser: elastic
        elasticPassword: changeme
        elasticsearch:
          rest:
            hostNames: ${ES_HOST:10.1.10.178}:${ES_PORT:5200}
          dateFormat: yyyy.MM.dd
          timeZone: Asia/Shanghai
          showTemplate: true
          discoverHost: false
        dslfile:
          refreshInterval: -1
        http:
          timeoutConnection: 5000
          timeoutSocket: 5000
          connectionRequestTimeout: 5000
          retryTime: 1
          maxLineLength: -1
          maxHeaderCount: 200
          maxTotal: 400
          defaultMaxPerRoute: 200
          soReuseAddress: false
          soKeepAlive: false
          timeToLive: 3600000
          keepAlive: 3600000
          keystore:
          keyPassword:
          hostnameVerifier:
```

* UI界面配置

![ES数据源配置](../../_media/plugin/es-datasource.png ':size=60%')

## 使用

> `默认数据源`操作

```js
// 定义Mapping结构
let indice = `{
    "mappings": {
        "properties": {
            "id":{
                "type":"long"
            },
            "name": {
                "type": "text"

            },
            "create_time": {
                "type": "date",
                "format":"yyyy-MM-dd HH:mm:ss"
            }
        }
    }
}`;
// 创建一个example名称的IndiceMapping
this.es.createIndiceMapping("example", indice);

// 添加Document数据
this.es.addDocuments("example", [
    {"id": 5, "name": "测试数据5", create_time: '2023-05-04 11:12:00'},
    {"id": 6, "name": "测试数据6", create_time: '2023-05-04 11:12:00'}
]);

// 查询Document数据
return this.es.getDocumentByFieldLike("example", "name", "测试")
```

> `指定数据源`操作：通过提供的`es`方法指定需要访问的数据源

```js
// 定义Mapping结构
let indice = `{
    "mappings": {
        "properties": {
            "id":{
                "type":"long"
            },
            "name": {
                "type": "text"

            },
            "create_time": {
                "type": "date",
                "format":"yyyy-MM-dd HH:mm:ss"
            }
        }
    }
}`;
// 通过`es`方法指定数据源，创建一个example名称的IndiceMapping
this.es.es("dpra-dev").createIndiceMapping("example", indice);

// 通过`es`方法指定数据源，添加Document数据
this.es.es("dpra-dev").addDocuments("example", [
    {"id": 5, "name": "测试数据5", create_time: '2023-05-04 11:12:00'},
    {"id": 6, "name": "测试数据6", create_time: '2023-05-04 11:12:00'}
]);

// 通过`es`方法指定数据源，查询Document数据
return this.es.es("dpra-dev").getDocumentByFieldLike("example", "name", "测试")
```