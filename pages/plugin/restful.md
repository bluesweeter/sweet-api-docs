## 介绍
***

> `RESTful插件`是插件模块，需要在【插件管理】中安装插件才能使用。

`RESTful插件`提供第三方API、WebService或具有访问路径的URL数据源访问API，更多API请参考：[RESTful API ](/pages/api/restful)

```text
RESTfulModule提供操作RESTful的接口，用于请求远程API或WebService，注意：
1、使用时一定是先调用connect(url)接口方法；
2、如果想指定数据源中的RESTful，则调用connect(key, url)接口方法先；
```

## 数据源配置
***

> UI界面上尚未提供，后续将完善该功能。

## 使用

> `默认数据源`操作

```js
// 调用API接口
let content = this.RESTful
    .connect("https://api.seniverse.com/v3/weather/daily.json")
    .param({key: 'SGzFb5IwLVq5562rF', location: 'guangzhou', language: 'zh-Hans', unit: 'c', start: -1, days: 5})
    .contentType("application/json")
    .header("Accept-Charset", "utf-8")
    .get()
    .getBody();

// 调用WebService接口
let xmlBody = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <getWeatherbyCityName xmlns="http://WebXml.com.cn/">
      <theCityName>54511</theCityName>
    </getWeatherbyCityName>
  </soap:Body>
</soap:Envelope>`;
let xmlResponse = this.RESTful
    .connect("http://www.webxml.com.cn/WebServices/WeatherWebService.asmx")
    .param({wsdl: ''})
    .contentType("text/xml;charset=UTF-8")
    .body(xmlBody)
    .post()
    .getBody();
```

> `指定数据源`操作：通过提供的`connect`方法指定`KEY`设置需要访问的数据源

```js
// 通过`connect`方法指定KEY，调用API接口
let content = this.RESTful
    .connect("weather", "/v3/weather/daily.json")
    .param({key: 'SGzFb5IwLVq5562rF', location: 'guangzhou', language: 'zh-Hans', unit: 'c', start: -1, days: 5})
    .contentType("application/json")
    .header("Accept-Charset", "utf-8")
    .get()
    .getBody();

// 通过`connect`方法指定KEY，调用WebService接口
let xmlBody = `<?xml version="1.0" encoding="utf-8"?>
<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <getWeatherbyCityName xmlns="http://WebXml.com.cn/">
      <theCityName>54511</theCityName>
    </getWeatherbyCityName>
  </soap:Body>
</soap:Envelope>`;
let xmlResponse = this.RESTful
    .connect("weatherWebService", "/WeatherWebService.asmx")
    .param({wsdl: ''})
    .contentType("text/xml;charset=UTF-8")
    .body(xmlBody)
    .post()
    .getBody();
```